<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('serial_numbers', function (Blueprint $table) {
            $table->id();
            $table->string("serial_number");
            $table->string("name")->nullable();
            $table->string("model_name");
            $table->string("part_number");
            $table->string("image_path");
            $table->string("warranty_term")->nullable();
            $table->date("warranty_expire")->nullable();
            $table->date("purchase_date")->nullable();
            $table->text("warranty_note")->nullable();
            $table->string("battery_chemistry", 50)->nullable();
            $table->string("battery_number_cells", 50)->nullable();
            $table->string("battery_energy", 50)->nullable();
            $table->string("battery_max_runtime", 30)->nullable();
            $table->string("bid_sound_mode", 50)->nullable();
            $table->string("bid_speakers", 50)->nullable();
            $table->string("bid_microphone", 50)->nullable();
            $table->string("bid_number_speakers", 50)->nullable();
            $table->string("bid_fingerprint", 50)->nullable();
            $table->string("bid_front_camera", 50)->nullable();
            $table->string("bid_front_camera_res", 50)->nullable();
            $table->string("bid_number_microphones", 5)->nullable();
            $table->string("i_hdmi", 50)->nullable();
            $table->string("i_vga", 50)->nullable();
            $table->string("i_rj45", 50)->nullable();
            $table->string("i_audio_li", 50)->nullable();
            $table->string("i_audio_lo", 50)->nullable();
            $table->string("i_hdmi_count", 5)->nullable();
            $table->string("i_usb_total_count", 5)->nullable();
            $table->string("i_usb_c", 50)->nullable();
            $table->string("i_headphone_micro", 50)->nullable();
            $table->string("i_usb_a_count", 5)->nullable();
            $table->string("dg_aspect_ratio", 50)->nullable();
            $table->string("dg_screen_resolution", 50)->nullable();
            $table->string("dg_screen_size", 50)->nullable();
            $table->string("dg_screen_mode", 50)->nullable();
            $table->string("dg_hdcp_support", 50)->nullable();
            $table->string("dg_touchscreen", 50)->nullable();
            $table->string("dg_standard_rr", 50)->nullable();
            $table->string("dg_gc_manufacturer", 50)->nullable();
            $table->string("dg_gc_model", 50)->nullable();
            $table->string("dg_gc_accessibility", 50)->nullable();
            $table->string("dg_ds_type", 5)->nullable();
            $table->text("dg_ds_tech")->nullable();
            $table->text("dg_bl_tech")->nullable();
            $table->string("id_keyboard", 50)->nullable();
            $table->string("id_keyboard_backlight", 50)->nullable();
            $table->string("id_pd_type", 30)->nullable();
            $table->text("id_tp_feat")->nullable();
            $table->text("id_keyboard_locale")->nullable();
            $table->string("id_num_pad", 50)->nullable();
            $table->string("m_card_reader", 50)->nullable();
            $table->string("m_installed_ram", 50)->nullable();
            $table->string("m_supported_ram", 50)->nullable();
            $table->string("m_ram_tech", 20)->nullable();
            $table->string("m_ram_installed_slots_count", 50)->nullable();
            $table->string("m_ram_total_slots_count", 50)->nullable();
            $table->string("m_ram_installed_slot_size", 50)->nullable();
            $table->text("misc_pack_contents")->nullable();
            $table->text("misc_security_feat")->nullable();
            $table->string("misc_country_origin", 20)->nullable();
            $table->string("misc_energy_star", 50)->nullable();
            $table->string("nc_bt", 20)->nullable();
            $table->string("nc_wlan", 20)->nullable();
            $table->text("nc_wlan_standard")->nullable();
            $table->text("nc_bt_standard")->nullable();
            $table->text("nc_ethernet_tech")->nullable();
            $table->string("pc_weight", 20)->nullable();
            $table->string("pc_height", 20)->nullable();
            $table->string("pc_width", 20)->nullable();
            $table->string("pc_depth", 20)->nullable();
            $table->string("pc_color", 20)->nullable();
            $table->string("power_max_wattage", 50)->nullable();
            $table->string("storage_ssd_interface", 50)->nullable();
            $table->string("storage_ssd_t_capacity", 50)->nullable();
            $table->string("storage_ssd_ff", 20)->nullable();
            $table->string("proc_cache", 20)->nullable();
            $table->string("proc_speed", 20)->nullable();
            $table->text("proc_type")->nullable();
            $table->text("proc_model")->nullable();
            $table->text("proc_core")->nullable();
            $table->text("proc_manufacturer")->nullable();
            $table->string("proc_max_turbo_speed", 20)->nullable();
            $table->text("proc_gen")->nullable();
            $table->text("proc_flow_count")->nullable();
            $table->string("os", 50)->nullable();
            $table->text("os_platform")->nullable();
            $table->text("comments")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('serial_numbers');
    }
};
