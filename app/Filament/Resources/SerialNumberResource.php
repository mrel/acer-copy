<?php

namespace App\Filament\Resources;

use App\Filament\Resources\SerialNumberResource\Pages;

use App\Models\SerialNumber;
use Carbon\Carbon;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Support\Str;
use Livewire\TemporaryUploadedFile;

class SerialNumberResource extends Resource
{
    protected static ?string $model = SerialNumber::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Section::make("Base information")
                    ->columns(1)
                    ->schema([
                        Forms\Components\Grid::make(2)->schema([
                            Forms\Components\Grid::make()
                                ->columnSpan(1)
                                ->schema([
                                    Forms\Components\TextInput::make('serial_number')
                                        ->label(__('fields_name.serial_number'))
                                        ->label(__("fields_name.serial_number"))
                                        ->required()
                                        ->maxLength(255)
                                        ->columnSpan(1),
                                    Forms\Components\TextInput::make('model_name')
                                        ->label(__('fields_name.model_name'))
                                        ->required()
                                        ->maxLength(255)
                                        ->columnSpan(1),
                                    Forms\Components\TextInput::make('part_number')
                                        ->label(__('fields_name.part_number'))
                                        ->required()
                                        ->maxLength(255)
                                        ->columnSpan(1),
                                    Forms\Components\Fieldset::make("Warranty")->schema([
                                        Forms\Components\DatePicker::make('warranty_expire')->default(Carbon::now()->addYears(3)->toString()),
                                        Forms\Components\DatePicker::make('purchase_date')->default(Carbon::now()->toString()),
                                    ])
                                ]),
                            Forms\Components\Grid::make()
                                ->columnSpan(1)
                                ->schema([
                                    Forms\Components\FileUpload::make('image_path')
                                        ->disk("public")
                                        ->label(__("fields_name.image_path"))
                                        ->directory("serial_number_images")
                                        ->required()
                                        ->image()
                                        ->getUploadedFileNameForStorageUsing(function (TemporaryUploadedFile $file): string {
                                            $extension = array_reverse(explode(".", $file->getFilename()))[0];
                                            return (string)str(Str::uuid())->prepend('serial-number-')->append("." . $extension);
                                        })
                                        ->columnSpanFull(),
                                ])
                        ])

                    ]),
                Forms\Components\Tabs::make("test")
                    ->schema([
                        Forms\Components\Tabs\Tab::make("Battery")->schema([
                            Forms\Components\TextInput::make('battery_chemistry')
                                ->label(__('fields_name.battery_chemistry'))
                                ->maxLength(50),
                            Forms\Components\TextInput::make('battery_number_cells')
                                ->label(__('fields_name.battery_number_cells'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('battery_energy')
                                ->label(__('fields_name.battery_energy'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('battery_max_runtime')
                                ->label(__('fields_name.battery_max_runtime'))
                                ->maxLength(30),
                        ]),
                        Forms\Components\Tabs\Tab::make("Built-in devices")->schema([
                            Forms\Components\TextInput::make('bid_sound_mode')
                                ->label(__('fields_name.bid_sound_mode'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('bid_speakers')
                                ->label(__('fields_name.bid_speakers'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('bid_number_speakers')
                                ->label(__('fields_name.bid_number_speakers'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('bid_number_microphones')
                                ->label(__('fields_name.bid_number_microphones'))
                                ->maxLength(5),
                            Forms\Components\TextInput::make('bid_microphone')
                                ->label(__('fields_name.bid_microphone'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('bid_fingerprint')
                                ->label(__('fields_name.bid_fingerprint'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('bid_front_camera')
                                ->label(__('fields_name.bid_front_camera'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('bid_front_camera_res')
                                ->label(__('fields_name.bid_front_camera_res'))
                                ->maxLength(10),
                        ]),
                        Forms\Components\Tabs\Tab::make("Interfaces/Ports")->schema([
                            Forms\Components\TextInput::make('i_hdmi')
                                ->label(__('fields_name.i_hdmi'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('i_rj45')
                                ->label(__('fields_name.i_rj45'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('i_audio_li')
                                ->label(__('fields_name.i_audio_li'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('i_audio_lo')
                                ->label(__('fields_name.i_audio_lo'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('i_hdmi_count')
                                ->label(__('fields_name.i_hdmi_count'))
                                ->maxLength(5),
                            Forms\Components\TextInput::make('i_usb_total_count')
                                ->label(__('fields_name.i_usb_total_count'))
                                ->maxLength(5),
                            Forms\Components\TextInput::make('i_headphone_micro')
                                ->label(__('fields_name.i_headphone_micro'))
                                ->tel()
                                ->maxLength(10),
                            Forms\Components\TextInput::make('i_usb_a_count')
                                ->label(__('fields_name.i_usb_a_count'))
                                ->maxLength(5),
                        ]),
                        Forms\Components\Tabs\Tab::make("Display & Graphics")->schema([
                            Forms\Components\TextInput::make('dg_aspect_ratio')
                                ->label(__('fields_name.dg_aspect_ratio'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('dg_screen_resolution')
                                ->label(__('fields_name.dg_screen_resolution'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('dg_screen_size')
                                ->label(__('fields_name.dg_screen_size'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('dg_hdcp_support')
                                ->label(__('fields_name.dg_hdcp_support'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('dg_touchscreen')
                                ->label(__('fields_name.dg_touchscreen'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('dg_standard_rr')
                                ->label(__('fields_name.dg_standard_rr'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('dg_gc_manufacturer')
                                ->label(__('fields_name.dg_gc_manufacturer'))
                                ->maxLength(50),
                            Forms\Components\TextInput::make('dg_gc_model')
                                ->label(__('fields_name.dg_gc_model'))
                                ->maxLength(50),
                            Forms\Components\TextInput::make('dg_gc_accessibility')
                                ->label(__('fields_name.dg_gc_accessibility'))
                                ->maxLength(50),
                            Forms\Components\TextInput::make('dg_ds_type')
                                ->label(__('fields_name.dg_ds_type'))
                                ->maxLength(5),
                            Forms\Components\TextInput::make('dg_screen_mode')
                                ->label(__('fields_name.dg_screen_mode'))
                                ->maxLength(10),
                            Forms\Components\Textarea::make('dg_ds_tech')
                                ->label(__("fields_name.dg_ds_tech"))
                                ->maxLength(65535),
                            Forms\Components\Textarea::make('dg_bl_tech')
                                ->label(__("fields_name.dg_bl_tech"))
                                ->maxLength(65535),
                        ])->columns(3),
                        Forms\Components\Tabs\Tab::make("Input devices")->schema([
                            Forms\Components\TextInput::make('id_keyboard')
                                ->label(__('fields_name.id_keyboard'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('id_pd_type')
                                ->label(__('fields_name.id_pd_type'))
                                ->maxLength(30),
                            Forms\Components\Textarea::make('id_tp_feat')
                                ->label(__("fields_name.id_tp_feat"))
                                ->maxLength(65535),
                            Forms\Components\Textarea::make('id_keyboard_locale')
                                ->label(__("fields_name.id_keyboard_locale"))
                                ->maxLength(65535),
                            Forms\Components\TextInput::make('id_num_pad')
                                ->label(__('fields_name.id_num_pad'))
                                ->maxLength(10),
                        ]),
                        Forms\Components\Tabs\Tab::make("Memory")->schema([
                            Forms\Components\TextInput::make('m_card_reader')
                                ->label(__('fields_name.m_card_reader'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('m_installed_ram')
                                ->label(__('fields_name.m_installed_ram'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('m_supported_ram')
                                ->label(__('fields_name.m_supported_ram'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('m_ram_tech')
                                ->label(__('fields_name.m_ram_tech'))
                                ->maxLength(20),
                            Forms\Components\TextInput::make('m_ram_installed_slots_count')
                                ->label(__('fields_name.m_ram_installed_slots_count'))
                                ->maxLength(50),
                            Forms\Components\TextInput::make('m_ram_total_slots_count')
                                ->label(__('fields_name.m_ram_total_slots_count'))
                                ->maxLength(10),
                            Forms\Components\TextInput::make('m_ram_installed_slot_size')
                                ->label(__('fields_name.m_ram_installed_slot_size'))
                                ->maxLength(50),
                        ])->columns(3),
                        Forms\Components\Tabs\Tab::make("Misc")->schema([
                            Forms\Components\Textarea::make('misc_pack_contents')
                                ->label(__("fields_name.misc_pack_contents"))
                                ->maxLength(65535),
                            Forms\Components\Textarea::make('misc_security_feat')
                                ->label(__("fields_name.misc_security_feat"))
                                ->maxLength(65535),
                            Forms\Components\TextInput::make('misc_country_origin')
                                ->label(__('fields_name.misc_country_origin'))
                                ->maxLength(20),
                            Forms\Components\TextInput::make('misc_energy_star')
                                ->label(__('fields_name.misc_energy_star'))
                                ->maxLength(10),
                        ]),
                        Forms\Components\Tabs\Tab::make("Network & Communication")->schema([
                            Forms\Components\TextInput::make('nc_bt')
                                ->label(__('fields_name.nc_bt'))
                                ->maxLength(20),
                            Forms\Components\TextInput::make('nc_wlan')
                                ->label(__('fields_name.nc_wlan'))
                                ->maxLength(20),
                            Forms\Components\Textarea::make('nc_wlan_standard')
                                ->label(__("fields_name.nc_wlan_standard"))
                                ->maxLength(65535),
                            Forms\Components\Textarea::make('nc_bt_standard')
                                ->label(__("fields_name.nc_bt_standard"))
                                ->maxLength(65535),
                            Forms\Components\Textarea::make('nc_ethernet_tech')
                                ->label(__("fields_name.nc_ethernet_tech"))
                                ->maxLength(65535),
                        ]),
                        Forms\Components\Tabs\Tab::make("Physical Characteristics")->schema([
                            Forms\Components\TextInput::make('pc_weight')
                                ->label(__('fields_name.pc_weight'))
                                ->maxLength(20),
                            Forms\Components\TextInput::make('pc_height')
                                ->label(__('fields_name.pc_height'))
                                ->maxLength(20),
                            Forms\Components\TextInput::make('pc_width')
                                ->label(__('fields_name.pc_width'))
                                ->maxLength(20),
                            Forms\Components\TextInput::make('pc_depth')
                                ->label(__('fields_name.pc_depth'))
                                ->maxLength(20),
                            Forms\Components\TextInput::make('pc_color')
                                ->label(__('fields_name.pc_color'))
                                ->maxLength(20),
                        ]),
                        Forms\Components\Tabs\Tab::make("Power Description")->schema([
                            Forms\Components\TextInput::make('power_max_wattage')
                                ->label(__('fields_name.power_max_wattage'))
                                ->maxLength(50),
                        ]),
                        Forms\Components\Tabs\Tab::make("Storage")->schema([
                            Forms\Components\TextInput::make('storage_ssd_interface')
                                ->label(__('fields_name.storage_ssd_interface'))
                                ->maxLength(50),
                            Forms\Components\TextInput::make('storage_ssd_t_capacity')
                                ->label(__('fields_name.storage_ssd_t_capacity'))
                                ->maxLength(50),
                            Forms\Components\TextInput::make('storage_ssd_ff')
                                ->label(__('fields_name.storage_ssd_ff'))
                                ->maxLength(20),
                        ]),
                        Forms\Components\Tabs\Tab::make("Processor")->schema([
                            Forms\Components\TextInput::make('proc_cache')
                                ->label(__('fields_name.proc_cache'))
                                ->maxLength(20),
                            Forms\Components\TextInput::make('proc_speed')
                                ->label(__('fields_name.proc_speed'))
                                ->maxLength(20),
                            Forms\Components\Textarea::make('proc_type')
                                ->label(__("fields_name.proc_type"))
                                ->maxLength(65535),
                            Forms\Components\Textarea::make('proc_model')
                                ->label(__("fields_name.proc_model"))
                                ->maxLength(65535),
                            Forms\Components\Textarea::make('proc_core')
                                ->label(__("fields_name.proc_core"))
                                ->maxLength(65535),
                            Forms\Components\Textarea::make('proc_manufacturer')
                                ->label(__("fields_name.proc_manufacturer"))
                                ->maxLength(65535),
                            Forms\Components\TextInput::make('proc_max_turbo_speed')
                                ->label(__('fields_name.proc_max_turbo_speed'))
                                ->maxLength(20),
                            Forms\Components\Textarea::make('proc_gen')
                                ->label(__("fields_name.proc_gen"))
                                ->maxLength(65535),
                        ]),
                        Forms\Components\Tabs\Tab::make("Operating System")->schema([
                            Forms\Components\TextInput::make('os')
                                ->label(__('fields_name.os'))
                                ->maxLength(50),
                            Forms\Components\Textarea::make('os_platform')
                                ->label(__("fields_name.os_platform"))
                                ->maxLength(65535),
                        ]),
                        Forms\Components\Tabs\Tab::make("Additional")->schema([
                            Forms\Components\Textarea::make('comments')
                                ->label(__("fields_name.comments"))
                                ->maxLength(65535),
                        ])
                    ])
                    ->columnSpan("full"),

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id'),
                Tables\Columns\ImageColumn::make('image_path'),
                Tables\Columns\TextColumn::make('serial_number')->searchable(),
                Tables\Columns\TextColumn::make('model_name')->searchable(),
                Tables\Columns\TextColumn::make('comments')->default("-"),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()->sortable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\Action::make('Копіювати')->action(function (SerialNumber $record) {
                    $newItem = $record->replicate();
                    $newItem->serial_number .= Str::uuid()->toString();
                    $newItem->push();
                }),
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSerialNumbers::route('/'),
            'create' => Pages\CreateSerialNumber::route('/create'),
            'edit' => Pages\EditSerialNumber::route('/{record}/edit'),
            'import' => Pages\ImportSerialNumbers::route('/import'),
        ];
    }
}
