<?php

namespace App\Filament\Resources\SerialNumberResource\Import\Actions;

use Filament\Forms\Components\Component;
use Konnco\FilamentImport\Concerns\HasFieldHelper;
use Konnco\FilamentImport\Concerns\HasFieldMutation;
use Konnco\FilamentImport\Concerns\HasFieldPlaceholder;
use Konnco\FilamentImport\Concerns\HasFieldRequire;
use Konnco\FilamentImport\Concerns\HasFieldValidation;

class ImportField extends Component
{
    use HasFieldMutation;
    use HasFieldHelper;
    use HasFieldPlaceholder;
    use HasFieldRequire;
    use HasFieldValidation;

    public function __construct(private string $name)
    {
        $this->hasDefaultState = true;
    }

    public static function make(string $name): self
    {
        return new self($name);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
