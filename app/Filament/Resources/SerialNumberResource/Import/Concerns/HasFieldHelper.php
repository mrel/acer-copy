<?php

namespace App\Filament\Resources\SerialNumberResource\Import\Concerns;

trait HasFieldHelper
{
    protected ?string $helperText = null;

    /**
     * @return $this
     */
    public function helperText($text): static
    {
        $this->helperText = $text;

        return $this;
    }

    public function getHelperText(): ?string
    {
        return $this->helperText;
    }
}
