<?php

namespace App\Filament\Resources\SerialNumberResource\Import\Concerns;

trait HasActionUniqueField
{
    protected bool|string $uniqueField = false;

    public function uniqueField(bool|string $uniqueField): static
    {
        $this->uniqueField = $uniqueField;

        return $this;
    }
}
