<?php

namespace App\Filament\Resources\SerialNumberResource\Pages;

use App\Filament\Resources\SerialNumberResource;
use Carbon\Carbon;
use Filament\Forms\Components\TextInput;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;
use App\Filament\Resources\SerialNumberResource\Import\Actions\ImportAction;
use App\Filament\Resources\SerialNumberResource\Import\Actions\ImportField;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ListSerialNumbers extends ListRecords
{
    protected static string $resource = SerialNumberResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
            ImportAction::make()
                ->fields([
                    ImportField::make('serial_number')->default(0)
                        ->label('Serial Number')->required(),
                    ImportField::make('name')
                        ->label('Name')->default(1),
                    ImportField::make('model_name')
                        ->label('Model name')->default(2),
                    ImportField::make("part_number")
                        ->label("Part number")->default(3),
                    ImportField::make("purchase_date")
                        ->label("Purchase date")->default(4),
                    ImportField::make("warranty_term")
                        ->label("Warranty term")->default(5),
                    ImportField::make("os")
                        ->label("OS")->default(6),
                    ImportField::make("proc_manufacturer")
                        ->label("Processor manufacturer")->default(7),
                    ImportField::make("proc_type")
                        ->label("Processor type")->default(8),
                    ImportField::make("proc_model")
                        ->label("Processor model")->default(9),
                    ImportField::make("proc_speed")
                        ->label("Processor speed")->default(10),
                    ImportField::make("proc_core")
                        ->label("Processor core")->default(11),
                    ImportField::make("proc_flow_count")
                        ->label("Processor flow count")->default(12),
                    ImportField::make("proc_gen")
                        ->label("Processor generation")->default(13),
                    ImportField::make("dg_gc_manufacturer")
                        ->label("Graphic adapter manufacturer")->default(14),
                    ImportField::make("dg_gc_model")
                        ->label("Graphic adapter model")->default(15),
                    ImportField::make("dg_screen_size")
                        ->label("Screen size")->default(16),
                    ImportField::make("dg_ds_type")
                        ->label("Screen type")->default(17),
                    ImportField::make("dg_bl_tech")
                        ->label("Backlight Technology")->default(18),
                    ImportField::make("dg_ds_tech")
                        ->label("Display Tech")->default(19),
                    ImportField::make("dg_screen_resolution")
                        ->label("Screen resolution")->default(20),
                    ImportField::make("dg_standard_rr")
                        ->label("Screen refresh rate")->default(21),
                    ImportField::make("m_installed_ram")
                        ->label("Installed ram")->default(22),
                    ImportField::make("m_ram_tech")
                        ->label("RAM Tech")->default(23),
                    ImportField::make("m_card_reader")
                        ->label("Card reader")->default(24),
                    ImportField::make("storage_ssd_t_capacity")
                        ->label("SSD Capacity")->default(25),
                    ImportField::make("storage_ssd_interface")
                        ->label("SSD Interface")->default(26),
                    ImportField::make("nc_wlan_standard")
                        ->label("WLAN Standard")->default(27),
                    ImportField::make("nc_ethernet_tech")
                        ->label("Ethernet Tech")->default(28),
                    ImportField::make("nc_bt_standard")
                        ->label("BT Standard")->default(29),
                    ImportField::make("bid_microphone")
                        ->label("Microphone")->default(30),
                    ImportField::make("bid_fingerprint")
                        ->label("Fingerprint reader")->default(31),
                    ImportField::make("bid_number_speakers")
                        ->label("Number of speakers")->default(32),
                    ImportField::make("bid_sound_mode")
                        ->label("Sound mode")->default(33),
                    ImportField::make("i_hdmi")
                        ->label("HDMI")->default(34),
                    ImportField::make("i_vga")
                        ->label("VGA")->default(35),
                    ImportField::make("i_usb_total_count")
                        ->label("USB Total Count")->default(36),
                    ImportField::make("i_usb_c")
                        ->label("USB-C")->default(37),
                    ImportField::make("i_rj45")
                        ->label("RJ-45")->default(38),
                    ImportField::make("id_keyboard")
                        ->label("Keyboard")->default(39),
                    ImportField::make("id_keyboard_backlight")
                        ->label("Keyboard backlight")->default(40),
                    ImportField::make("battery_energy")
                        ->label("Battery energy")->default(41),
                    ImportField::make("pc_height")
                        ->label("Height")->default(42),
                    ImportField::make("pc_width")
                        ->label("Width")->default(43),
                    ImportField::make("pc_depth")
                        ->label("Depth")->default(44),
                    ImportField::make("pc_weight")
                        ->label("Weight")->default(45),
                    ImportField::make("misc_security_feat")
                        ->label("Other")->default(46),
                    ImportField::make("image_path")
                        ->label("Product Image")->default(47),
                ], 2)->mutateBeforeCreate(function ($data) {
                    $fileExt = pathinfo(parse_url($data["image_path"], PHP_URL_PATH), PATHINFO_EXTENSION);
                    $filePath = "serial_number_images/{$data["serial_number"]}.{$fileExt}";
                    Storage::disk("public")->put($filePath, file_get_contents($data["image_path"]));

                    $purchaseDate = Carbon::createFromFormat('Y-m-d', $data["purchase_date"] ?? now()->format("Y-m-d"));

                    $data["purchase_date"] = $purchaseDate->clone();
                    $data["warranty_expire"] = $purchaseDate->addMonths(intval($data["warranty_term"]));

                    $data["image_path"] = $filePath;
                    return $data;
                })
        ];
    }
}
