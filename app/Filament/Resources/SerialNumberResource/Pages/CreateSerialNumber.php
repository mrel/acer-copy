<?php

namespace App\Filament\Resources\SerialNumberResource\Pages;

use App\Filament\Resources\SerialNumberResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateSerialNumber extends CreateRecord
{
    protected static string $resource = SerialNumberResource::class;
}
