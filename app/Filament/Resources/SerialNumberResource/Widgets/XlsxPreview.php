<?php

namespace App\Filament\Resources\SerialNumberResource\Widgets;

use Filament\Widgets\Widget;

class XlsxPreview extends Widget
{
    public array $data = [];

    protected static string $view = 'filament.resources.serial-number-resource.widgets.xlsx-preview';

}
