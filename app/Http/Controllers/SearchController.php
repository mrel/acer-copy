<?php

namespace App\Http\Controllers;

use App\Models\SerialNumber;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(string $searchQuery)
    {
        try {
            $serialNumber = SerialNumber::where("serial_number", $searchQuery)->firstOrFail();

            return view("search-result", [
                "product" => $serialNumber
            ]);
        } catch (\Throwable) {
            return view("not-found");
        }
    }
}
