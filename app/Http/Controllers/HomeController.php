<?php

namespace App\Http\Controllers;

use App\Models\SerialNumber;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $request->flash();

        $searchQuery = $request->get("search", false);

        if (!$searchQuery) {
            return view("home");
        }

        try {
            $serialNumber = SerialNumber::where("serial_number", $searchQuery)->firstOrFail();

            return view("search-result", [
                "product" => $serialNumber
            ]);
        } catch (\Throwable) {
            return view("not-found");
        }

    }
}
