<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SerialNumber extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getHasBatteryInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "battery_chemistry",
            "battery_number_cells",
            "battery_energy",
            "battery_max_runtime",
        ]);
    }

    public function getHasBuiltInDevicesInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "bid_sound_mode",
            "bid_speakers",
            "bid_microphone",
            "bid_number_speakers",
            "bid_fingerprint",
            "bid_front_camera",
            "bid_front_camera_res",
            "bid_number_microphones",
        ]);
    }

    public function getHasInterfacesInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "i_hdmi",
            "i_rj45",
            "i_audio_li",
            "i_audio_lo",
            "i_hdmi_count",
            "i_usb_total_count",
            "i_headphone_micro",
            "i_usb_a_count",
        ]);
    }

    public function getHasDisplayGraphicsInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "dg_aspect_ratio",
            "dg_screen_resolution",
            "dg_screen_size",
            "dg_screen_mode",
            "dg_hdcp_support",
            "dg_touchscreen",
            "dg_standard_rr",
            "dg_gc_manufacturer",
            "dg_gc_model",
            "dg_gc_accessibility",
            "dg_ds_type",
            "dg_ds_tech",
            "dg_bl_tech",
        ]);
    }

    public function getHasInputDeviceInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "id_keyboard",
            "id_pd_type",
            "id_tp_feat",
            "id_keyboard_locale",
            "id_num_pad",
        ]);
    }

    public function getHasMemoryInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "m_card_reader",
            "m_installed_ram",
            "m_supported_ram",
            "m_ram_tech",
            "m_ram_installed_slots_count",
            "m_ram_total_slots_count",
            "m_ram_installed_slot_size",
        ]);
    }

    public function getHasMiscInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "misc_pack_contents",
            "misc_security_feat",
            "misc_country_origin",
            "misc_energy_star",
        ]);
    }

    public function getHasNetworkInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "nc_bt",
            "nc_wlan",
            "nc_wlan_standard",
            "nc_bt_standard",
            "nc_ethernet_tech",
        ]);
    }

    public function getHasPhysicalInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "pc_weight",
            "pc_height",
            "pc_width",
            "pc_depth",
            "pc_color",
        ]);
    }

    public function getHasPowerInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "power_max_wattage",
        ]);
    }

    public function getHasStorageInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "storage_ssd_interface",
            "storage_ssd_t_capacity",
            "storage_ssd_ff",
        ]);
    }

    public function getHasProcessorInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "proc_cache",
            "proc_speed",
            "proc_type",
            "proc_model",
            "proc_core",
            "proc_manufacturer",
            "proc_max_turbo_speed",
            "proc_gen",
        ]);
    }

    public function getHasOsInformationAttribute(): bool
    {
        return $this->checkAnyAttributeFilled([
            "os",
            "os_platform",
        ]);
    }

    public function getIsWarrantyActiveAttribute(): bool
    {
        return Carbon::now()->lessThanOrEqualTo(Carbon::make($this->attributes["warranty_expire"]));
    }

    protected function checkAnyAttributeFilled(array $attributeKeys): bool
    {
        foreach ($attributeKeys as $attributeKey) {
            if (!empty($this->attributes[$attributeKey])) {
                return true;
            }
        }

        return false;
    }


}
