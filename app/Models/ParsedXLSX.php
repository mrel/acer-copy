<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Sushi\Sushi;

class ParsedXLSX extends Model
{
    use Sushi;

    public static $rowsTemp = [];

    public static function setRows(array $rows): self
    {
        self::$rowsTemp = $rows;
        return new self();
    }

    public function getRows()
    {
        return self::$rowsTemp;
    }
}
