@extends('layouts.main')

@section('content')
    <section class="section section--secondary section--secondary--pad-scroll light align-content-center text-center" prefix="q: http://www.sdl.com/web/schemas/core" typeof="q:QuickLinks">
        <header class="section__header">
            <h2 class="section__title" property="headline">Серійний номер не знайдено</h2>
        </header>
    </section>
@endsection
