@extends('layouts.main')

@section('content')
        <div class="region-main max-width-1366">
            <aside class="rail" role="navigation">
                <div class="slideout slideout--inline" id="slideout-left-rail">
                    <div class="slideout__inner slideout__inner--inline">
                        <nav class="nav-rail" typeof="Region" resource="Left Rail Navigation">
                            <header class="nav-rail__header" style="display: flex;justify-content: space-between;">
                                <h4 class="nav-rail__heading">Теми підтримки</h4>
                            </header>
                            <div class="nav-rail__body  accordion__item" id="navgationFilter">
                                <ul class="accordion">
                                    <li class="accordion__item" data-collapse="" data-collapsed=""
                                    >
                                        <div style="display: flex; align-items: center; justify-content: space-between;">
                                            <a target="_self" class="accordion__link"
                                               href="https://www.acer.com/ua-uk/support/drivers-and-manuals">Драйвери та
                                                посібники </a>
                                            <span class="material-icons" name="supportAccordion"
                                                  style="cursor: pointer; margin-right: 24px;">expand_more</span>
                                        </div>
                                        <ul class="menu-filter menu-filter--accordion">
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_blank" class="menu-filter__link "
                                                   href="https://community.acer.com/uk/kb/articles/15378-microsoft-system-center-configuration-manager-sccm"
                                                   rel="nofollow">Microsoft System Center Configuration Manager [SCCM]</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="accordion__item"
                                    >
                                        <a target="_blank" class="accordion__link " href="https://community.acer.com/uk"
                                           rel="nofollow">FAQ до продуктів Acer</a>
                                    </li>
                                    <li class="accordion__item " data-collapse="" data-collapsed=""
                                    >
                                        <div style="display: flex; align-items: center; justify-content: space-between;">
                                            <a target="_self" class="accordion__link"
                                               href="https://www.acer.com/ua-uk/account/registerproduct">Зареєструвати </a>
                                            <span class="material-icons" name="supportAccordion"
                                                  style="cursor: pointer; margin-right: 24px;">expand_more</span>
                                        </div>
                                        <ul class="menu-filter menu-filter--accordion">
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_self" class="menu-filter__link "
                                                   href="https://www.acer.com/ua-uk/account/registerproduct">Зареєструвати
                                                    виріб</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="accordion__item " data-collapse="" data-collapsed=""
                                    >
                                        <div style="display: flex; align-items: center; justify-content: space-between;">
                                            <a target="_self" class="accordion__link"
                                               href="https://www.acer.com/ua-uk/support/alerts-recalls">Сповіщення і
                                                нагадування </a>
                                            <span class="material-icons" name="supportAccordion"
                                                  style="cursor: pointer; margin-right: 24px;">expand_more</span>
                                        </div>
                                        <ul class="menu-filter menu-filter--accordion">
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_blank" class="menu-filter__link "
                                                   href="https://community.acer.com/uk/kb/articles/11278" rel="nofollow">Intel
                                                    Security Bulletins</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="accordion__item " data-collapse="" data-collapsed=""
                                    >
                                        <div style="display: flex; align-items: center; justify-content: space-between;">
                                            <a target="_self" class="accordion__link"
                                               href="https://www.acer.com/ua-uk/support/warranty/standard-warranty">Гарантія </a>
                                            <span class="material-icons" name="supportAccordion"
                                                  style="cursor: pointer; margin-right: 24px;">expand_more</span>
                                        </div>
                                        <ul class="menu-filter menu-filter--accordion">
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_self" class="menu-filter__link "
                                                   href="https://www.acer.com/ua-uk/support/warranty/standard-warranty">Стандартна
                                                    гарантія</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_blank" class="menu-filter__link "
                                                   href="https://community.acer.com/uk/kb/articles/15377-acer-international-travelers-warranty"
                                                   rel="nofollow">ITW (International traveller’s warranty) – міжнародна
                                                    гарантія мандрівника</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_self" class="menu-filter__link "
                                                   href="https://www.acer.com/ua-uk/support/warranty/additional-services-extended-warranty">Додаткова
                                                    гарантія</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="accordion__item"
                                    >
                                        <a target="_self" class="accordion__link "
                                           href="https://www.acer.com/ua-uk/support/warranty/pixel-policy">Несправні або
                                            "биті" пікселі</a>
                                    </li>
                                    <li class="accordion__item " data-collapse="" data-collapsed=""
                                    >
                                        <div style="display: flex; align-items: center; justify-content: space-between;">
                                            <a target="_self" class="accordion__link"
                                               href="https://www.acer.com/ua-uk/support/windows/windows-11-compatibility">Windows </a>
                                            <span class="material-icons" name="supportAccordion"
                                                  style="cursor: pointer; margin-right: 24px;">expand_more</span>
                                        </div>
                                        <ul class="menu-filter menu-filter--accordion">
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_self" class="menu-filter__link "
                                                   href="https://www.acer.com/ua-uk/support/windows/windows-11-compatibility">Сумісність
                                                    з Windows 11</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_blank" class="menu-filter__link "
                                                   href="https://community.acer.com/uk/kb/categories/3-windows"
                                                   rel="nofollow">Запитання і відповіді</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_self" class="menu-filter__link "
                                                   href="https://www.acer.com/ua-uk/support/windows/windows-autopilot">Windows
                                                    Autopilot</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="accordion__item " data-collapse="" data-collapsed=""
                                    >
                                        <div style="display: flex; align-items: center; justify-content: space-between;">
                                            <a target="_blank" class="accordion__link"
                                               href="https://community.acer.com/uk/kb/articles/57-locating-your-acer-snid-or-serial-number"
                                               rel="nofollow">Де розташований серійний номер? </a>
                                            <span class="material-icons" name="supportAccordion"
                                                  style="cursor: pointer; margin-right: 24px;">expand_more</span>
                                        </div>
                                        <ul class="menu-filter menu-filter--accordion">
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_blank" class="menu-filter__link "
                                                   href="https://community.acer.com/uk/kb/articles/63-where-to-locate-the-serial-number-on-an-acer-notebook-or-netbook"
                                                   rel="nofollow">Ноутбуки</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_blank" class="menu-filter__link "
                                                   href="https://community.acer.com/uk/kb/articles/62-locate-the-serial-number-on-an-acer-desktop-computer"
                                                   rel="nofollow">Настільні ПК</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_blank" class="menu-filter__link "
                                                   href="https://community.acer.com/uk/kb/articles/61-locate-the-serial-number-or-snid-number-on-an-acer-tablet"
                                                   rel="nofollow">Планшети</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_blank" class="menu-filter__link "
                                                   href="https://community.acer.com/uk/kb/articles/60-locate-the-serial-number-and-snid-on-your-acer-monitor"
                                                   rel="nofollow">Монітори</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_blank" class="menu-filter__link "
                                                   href="https://community.acer.com/uk/kb/articles/59-locate-the-serial-number-on-an-acer-smartphone"
                                                   rel="nofollow">Смартфони</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_blank" class="menu-filter__link "
                                                   href="https://community.acer.com/uk/kb/articles/58-locate-the-serial-number-located-on-an-acer-projector"
                                                   rel="nofollow">Проектори</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_blank" class="menu-filter__link "
                                                   href="https://community.acer.com/uk/kb/articles/10384-locate-the-serial-number-on-acer-windows-mixed-reality-headsets"
                                                   rel="nofollow">Відеошоломи змішаної реальності</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_blank" class="menu-filter__link "
                                                   href="https://community.acer.com/uk/kb/articles/15994-znaydit-seriyniy-nomer-na-acer-escooters"
                                                   rel="nofollow">eScooters</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="accordion__item"
                                    >
                                        <a target="_self" class="accordion__link "
                                           href="https://www.acer.com/ua-uk/support/contact-acer/service-contact">Список
                                            сервісних центрів Acer</a>
                                    </li>
                                    <li class="accordion__item " data-collapse="" data-collapsed=""
                                    >
                                        <div style="display: flex; align-items: center; justify-content: space-between;">
                                            <a target="_self" class="accordion__link"
                                               href="https://www.acer.com/ua-uk/support/warranty/additional-services">ДОДАТКОВІ
                                                Сервіси Acer </a>
                                            <span class="material-icons" name="supportAccordion"
                                                  style="cursor: pointer; margin-right: 24px;">expand_more</span>
                                        </div>
                                        <ul class="menu-filter menu-filter--accordion">
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_self" class="menu-filter__link "
                                                   href="https://www.acer.com/ua-uk/support/warranty/additional-services-upgrades">Модернізація/адаптація
                                                    комп’ютера відповідно до Ваших потреб</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_self" class="menu-filter__link "
                                                   href="https://www.acer.com/ua-uk/support/warranty/additional-services-prevention">Профілактика</a>
                                            </li>
                                            <li class="menu-filter__item"
                                            >
                                                <a target="_self" class="menu-filter__link "
                                                   href="https://www.acer.com/ua-uk/support/warranty/additional-services-accessories">Aксесуари
                                                    та запасні частини Acer</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </aside>
            <div class="main" role="main">

                <div class="view view-landing">
                    <div class="filter-control__group">
                        <div class="support filter-control__item filter-control--hide@md">
                            <a class="slideout-trigger filter-control__btn" role="button" aria-label="menu"
                               aria-expanded="false" data-open="panel" data-target="#slideout-left-rail">
                                <span class="material-icons">more_vert</span>Теми підтримки
                            </a>
                        </div>
                    </div>
                    <div id="support-notProducts-sections" style="display: block;">

                        <section class="section">
                            <div class="page">
                                <div class="page__body text-center">
                                    <h6>Ласкаво просимо на новий сайт служби підтримки Acer</h6>

                                    <div>Ми запускаємо новий сайт підтримки і просимо вас набратися терпіння, оскільки ми
                                        переносимо функції в онлайн. Якщо у вас виникнуть проблеми з пошуком потрібної
                                        інформації, відвідайте <a href="https://community.acer.com/" target="_blank">спільноту
                                            Acer</a> або сторінку <a href="https://community.acer.com/uk/kb/"
                                                                     target="_blank">Відповіді Acer</a>.
                                    </div>
                                    <div class="content-img">
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="section section--secondary section--secondary--pad-scroll light"
                                 prefix="q: http://www.sdl.com/web/schemas/core" typeof="q:QuickLinks">
                            <header class="section__header">
                                <h2 class="section__title" property="headline">Посилання для самодопомоги</h2>
                            </header>
                            <div class="section__body scrollbar" data-simplebar="init">
                                <div class="simplebar-wrapper" style="margin: 0px -24px;">
                                    <div class="simplebar-height-auto-observer-wrapper">
                                        <div class="simplebar-height-auto-observer"></div>
                                    </div>
                                    <div class="simplebar-mask">
                                        <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                                            <div class="simplebar-content-wrapper" tabindex="0" role="region"
                                                 aria-label="scrollable content" style="height: auto; overflow: hidden;">
                                                <div class="simplebar-content" style="padding: 0px 24px;">
                                                    <ul class="card-list card-list--scroll-3">
                                                        <li class="card-list__item">
                                                            <a target="" class="card card-icon box"
                                                               href="https://www.acer.com/ua-uk/support/drivers-and-manuals"
                                                            >
                                                                <div class="card-icon__img material-icons">menu_book</div>
                                                                <div class="card__body card-icon__body">
                                                                    <h5 class="card__title card-icon__title"
                                                                        property="linkText">Драйвери і посібники</h5>
                                                                </div>
                                                            </a>
                                                        </li>
                                                        <li class="card-list__item">
                                                            <a target="_blank" class="card card-icon box"
                                                               href="https://community.acer.com/uk/kb" rel="nofollow"
                                                            >
                                                                <div class="card-icon__img material-icons">question_answer
                                                                </div>
                                                                <div class="card__body card-icon__body">
                                                                    <h5 class="card__title card-icon__title"
                                                                        property="linkText">Відповіді Acer</h5>
                                                                </div>
                                                            </a>
                                                        </li>
                                                        <li class="card-list__item">
                                                            <a target="_blank" class="card card-icon box"
                                                               href="https://community.acer.com/" rel="nofollow"
                                                            >
                                                                <div class="card-icon__img material-icons">group</div>
                                                                <div class="card__body card-icon__body">
                                                                    <h5 class="card__title card-icon__title"
                                                                        property="linkText">Спільнота Acer</h5>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="simplebar-placeholder" style="width: 977px; height: 220px;"></div>
                                </div>
                                <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
                                    <div class="simplebar-scrollbar simplebar-visible"
                                         style="width: 0px; display: none;"></div>
                                </div>
                                <div class="simplebar-track simplebar-vertical" style="visibility: hidden;">
                                    <div class="simplebar-scrollbar simplebar-visible"
                                         style="height: 0px; display: none;"></div>
                                </div>
                            </div>
                        </section>


                        <section class="section section--secondary section--secondary--pad-scroll light"
                                 prefix="q: http://www.sdl.com/web/schemas/core" typeof="q:QuickLinks">
                            <header class="section__header">
                                <h2 class="section__title" property="headline">Гарантійне обслуговування та ремонт</h2>
                            </header>
                            <div class="section__body scrollbar" data-simplebar="init">
                                <div class="simplebar-wrapper" style="margin: 0px -24px;">
                                    <div class="simplebar-height-auto-observer-wrapper">
                                        <div class="simplebar-height-auto-observer"></div>
                                    </div>
                                    <div class="simplebar-mask">
                                        <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                                            <div class="simplebar-content-wrapper" tabindex="0" role="region"
                                                 aria-label="scrollable content" style="height: auto; overflow: hidden;">
                                                <div class="simplebar-content" style="padding: 0px 24px;">
                                                    <ul class="card-list card-list--scroll-3">
                                                        <li class="card-list__item">
                                                            <a target="_blank" class="card card-icon box"
                                                               href="https://ua.answers.acer.com/app/case_status"
                                                               rel="nofollow"
                                                            >
                                                                <div class="card-icon__img material-icons">build</div>
                                                                <div class="card__body card-icon__body">
                                                                    <h5 class="card__title card-icon__title"
                                                                        property="linkText">Статус справи про ремонт</h5>
                                                                </div>
                                                            </a>
                                                        </li>
                                                        <li class="card-list__item">
                                                            <a target="" class="card card-icon box"
                                                               href="https://www.acer.com/ua-uk/account/registerproduct"
                                                            >
                                                                <div class="card-icon__img material-icons">devices</div>
                                                                <div class="card__body card-icon__body">
                                                                    <h5 class="card__title card-icon__title"
                                                                        property="linkText">Статус справи про ремонт</h5>
                                                                </div>
                                                            </a>
                                                        </li>
                                                        <li class="card-list__item">
                                                            <a target="" class="card card-icon box"
                                                               href="https://www.acer.com/ua-uk/support/warranty/additional-services"
                                                            >
                                                                <div class="card-icon__img material-icons">policy</div>
                                                                <div class="card__body card-icon__body">
                                                                    <h5 class="card__title card-icon__title"
                                                                        property="linkText">Acer Care Plus</h5>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="simplebar-placeholder" style="width: 977px; height: 220px;"></div>
                                </div>
                                <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
                                    <div class="simplebar-scrollbar simplebar-visible"
                                         style="width: 0px; display: none;"></div>
                                </div>
                                <div class="simplebar-track simplebar-vertical" style="visibility: hidden;">
                                    <div class="simplebar-scrollbar simplebar-visible"
                                         style="height: 0px; display: none;"></div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
@endsection
