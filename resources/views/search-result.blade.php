@extends('layouts.main')

@section('stack-scripts')
    <script>
        window.onload = function () {
            document.querySelectorAll('.accordion__link').forEach(function (el) {
                console.log(el);
                el.addEventListener("click", function (e) {
                    console.log(e);
                    e.target.classList.toggle("supportLink-checked");
                    document.querySelector('.specs').classList.toggle("active");
                })
            })
        }
    </script>
@endsection
@section('content')
    <style>
        .warranty-active {
            color: #80c343;
        }

        .warranty-end {
            color: #c34356;
        }

        .specs {
            display: none;
        }

        .specs.active {
            display: block;
        }
        
        .accordion__link {
            cursor: pointer;
        }
    </style>

    <div class="region-main max-width-1366">
        <aside class="rail" role="navigation">
            <div class="slideout slideout--inline" id="slideout-left-rail">
                <div class="slideout__inner slideout__inner--inline">


                    <nav class="nav-rail" typeof="Region" resource="Left Rail Navigation">
                        <header class="nav-rail__header" style="display: flex;justify-content: space-between;">
                            <h4 class="nav-rail__heading">Теми підтримки</h4>
                        </header>
                        <div class="nav-rail__body  accordion__item" id="navgationFilter">
                            <ul class="accordion">
                                <li class="accordion__item">
                                    <a class="accordion__link">Деталі продукту</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </aside>
        <div class="main" role="main">

            <div class="view view-landing">
                <div class="filter-control__group">
                    <div class="support filter-control__item filter-control--hide@md">
                        <a class="slideout-trigger filter-control__btn" role="button" aria-label="menu"
                           aria-expanded="false" data-open="panel" data-target="#slideout-left-rail">
                            <span class="material-icons">more_vert</span>Теми підтримки
                        </a>
                    </div>
                </div>
                <section class="section section--secondary">
                    <div id="drives-manual-content" class="view view-results">
                        <header class="section__header">
                            <div class="card card--horizontal@md" href="#">
                                <img class="card__img card__img--horizontal@sm"
                                     src="{{asset(sprintf("storage/%s", $product->image_path))}}"
                                     alt="text goes here">
                                <div class="card__body card__body--center-left">
                                    <p class="card__overline">Підтримка виробу для</p>
                                    <h2 class="card__title">{{ $product->model_name }}</h2>
                                    <p class="card__text">Назва моделі: {{ $product->model_name }} | Номер частини:
                                        {{$product->part_number}} | Статус&nbsp;гарантії:
                                        @if($product->is_warranty_active)
                                            <span class="warranty-active">На гарантії</span>
                                        @else
                                            <span class="warranty-end">Термін вичерпано</span>
                                        @endif
                                    </p>
                                    <p class="card__text">
                                        Дата
                                        покупки: {{\Illuminate\Support\Carbon::make($product->purchase_date)->format("Y-m-d")}}
                                        | Гарантія дійсна
                                        до: {{\Illuminate\Support\Carbon::make($product->warranty_expire)->format("Y-m-d")}}
                                    </p>
                                </div>
                            </div>
                        </header>
                        <div class="specs">
                            @if($product->has_battery_information)
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Battery Information</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->battery_chemistry))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Battery Chemistry</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->battery_chemistry}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->battery_number_cells))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Number of Cells</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->battery_number_cells}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->battery_energy))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Battery Energy</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->battery_energy}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->battery_max_runtime))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Maximum Battery Run Time</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->battery_max_runtime}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </section>
                            @endif
                            @if($product->has_built_in_devices_information)
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Built-in Devices</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->bid_sound_mode))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Sound Mode</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->bid_sound_mode}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->bid_speakers))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Speakers</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->bid_speakers}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->bid_microphone))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Microphone</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->bid_microphone}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->bid_number_speakers))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Number of Speakers</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->bid_number_speakers}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->bid_fingerprint))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Finger Print Reader</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->bid_fingerprint}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->bid_front_camera))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Front Camera/Webcam</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->bid_front_camera}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->bid_front_camera_res))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Front Camera/Webcam Video Resolution</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->bid_front_camera_res}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->bid_number_microphones))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Number of Microphones</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->bid_number_microphones}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </section>
                            @endif
                            @if($product->has_interfaces_information)
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Interfaces/Ports</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->i_hdmi))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">HDMI</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->i_hdmi}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->i_rj45))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Network (RJ-45)</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->i_rj45}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->i_audio_li))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Audio Line In</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->i_audio_li}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->i_audio_lo))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Audio Line Out</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->i_audio_lo}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->i_hdmi_count))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Number of HDMI Outputs</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->i_hdmi_count}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->i_usb_total_count))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Total Number of USB Ports</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->i_usb_total_count}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->i_headphone_micro))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Headphone/Microphone Combo Port</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->i_headphone_micro}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->i_usb_a_count))

                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Number of USB 3.2 Gen 1 Type-A Ports</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->i_usb_a_count}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </section>
                            @endif
                            @if(!empty($product->has_dispaly_graphics_information))
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Display &amp; Graphics</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->dg_aspect_ratio))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Aspect Ratio</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_aspect_ratio}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->dg_screen_resolution))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Screen Resolution</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_screen_resolution}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->dg_screen_size))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Screen Size</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_screen_size}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->dg_screen_mode))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Screen Mode</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_screen_mode}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->dg_hdcp_support))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">HDCP Supported</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_hdcp_support}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->dg_touchscreen))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Touchscreen</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_touchscreen}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->dg_standard_rr))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Standard Refresh Rate</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_standard_rr}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->dg_gc_manufacturer))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Graphics Controller Manufacturer</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_gc_manufacturer}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->dg_gc_model))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Graphics Controller Model</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_gc_model}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->dg_gc_accessibility))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Graphics Memory Accessibility</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_gc_accessibility}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->dg_ds_type))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Display Screen Type</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_ds_type}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->dg_ds_tech))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Display Screen Technology</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_ds_tech}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->dg_bl_tech))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Backlight Technology</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->dg_bl_tech}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                </section>
                            @endif
                            @if($product->has_input_device_information)
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Input Devices</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->id_keyboard))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Keyboard</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->id_keyboard}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->id_pd_type))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Pointing Device Type</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->id_pd_type}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->id_tp_feat))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">TouchPad Features</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->id_tp_feat}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->id_keyboard_locale))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Keyboard Localization</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->id_keyboard_locale}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->id_num_pad))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Numeric Pad</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->id_num_pad}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                </section>
                            @endif
                            @if($product->has_memory_information)
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Memory</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->m_card_reader))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Memory Card Reader</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->m_card_reader}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->m_installed_ram))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Total Installed System Memory</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->m_installed_ram}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->m_supported_ram))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Maximum Supported System Memory</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->m_supported_ram}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->m_ram_tech))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">System Memory Technology</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->m_ram_tech}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->m_ram_installed_slots_count))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Installed Slot Memory Configuration</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->m_ram_installed_slots_count}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->m_ram_total_slots_count))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Total Number of Memory Slots</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->m_ram_total_slots_count}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->m_ram_installed_slot_size))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Installed Slot Memory</p>
                                                    <ul class="spec-table__td">
                                                        8 GB
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </section>
                            @endif
                            @if($product->has_misc_information)
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Miscellaneous</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->misc_pack_contents))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Package Contents</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->misc_pack_contents}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->misc_security_feat))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Security Features</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->misc_security_feat}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->misc_country_origin))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Country of Origin</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->misc_country_origin}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->misc_energy_star))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Energy Star</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->misc_energy_star}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </section>
                            @endif
                            @if($product->has_network_information)
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Network &amp; Communication</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->nc_bt))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Bluetooth</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->nc_bt}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->nc_wlan))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Wireless LAN</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->nc_wlan}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(!empty($product->nc_wlan_standard))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Wireless LAN Standard</p>
                                                    <ul class="spec-table__td">
                                                        IEEE 802.11ac
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->nc_bt_standard))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Bluetooth Standard</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->nc_bt_standard}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->nc_ethernet_tech))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Ethernet Technology</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->nc_ethernet_tech}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </section>
                            @endif
                            @if($product->has_physical_information)
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Physical Characteristics</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->pc_weight))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Weight (Approximate)</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->pc_weight}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->pc_height))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Height</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->pc_height}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->pc_width))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Width</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->pc_width}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->pc_depth))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Depth</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->pc_depth}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->pc_color))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Product Color</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->pc_color}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </section>
                            @endif
                            @if($product->has_power_information)
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Power Description</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->power_max_wattage))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Maximum Power Supply Wattage</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->power_max_wattage}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </section>
                            @endif
                            @if($product->has_storage_information)
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Storage</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->storage_ssd_interface))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Solid State Drive Interface</p>
                                                    <ul class="spec-table__td">
                                                        PCIe NVMe
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->storage_ssd_t_capacity))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Total Solid State Drive Capacity</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->storage_ssd_t_capacity}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->storage_ssd_ff))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">SSD Form Factor</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->storage_ssd_ff}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </section>
                            @endif
                            @if($product->has_processor_information)
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Processor</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->proc_cache))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Cache</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->proc_cache}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->proc_speed))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Processor Speed</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->proc_speed}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->proc_type))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Processor Type</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->proc_type}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->proc_model))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Processor Model</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->proc_model}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->proc_core))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Processor Core</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->proc_core}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->proc_manufacturer))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Processor Manufacturer</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->proc_manufacturer}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->proc_max_turbo_speed))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Maximum Turbo Speed</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->proc_max_turbo_speed}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->proc_gen))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Processor Generation</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->proc_gen}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </section>
                            @endif
                            @if($product->has_os_information)
                                <section class="spec-table" data-collapse="">
                                    <header class="spec-table__header" data-collapse-trigger="">
                                        <h6 class="spec-table__title">Operating System</h6>
                                        <span class="material-icons spec-table__icon"></span>
                                    </header>
                                    @if(!empty($product->os))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Operating System</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->os}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($product->os_platform))
                                        <div class="spec-table__body">
                                            <div class="spec-table__items">
                                                <div class="spec-table__item">
                                                    <p class="spec-table__th">Operating System Platform</p>
                                                    <ul class="spec-table__td">
                                                        {{$product->os_platform}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </section>
                            @endif
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
