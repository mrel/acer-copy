<x-filament::page>
    <x-filament::card>
        <form wire:submit.prevent="submit">
            {{ $this->form }}
            <div class="action-wrapper">
                <x-filament::button type="submit">
                    submit
                </x-filament::button>
            </div>
        </form>
    </x-filament::card>

</x-filament::page>
