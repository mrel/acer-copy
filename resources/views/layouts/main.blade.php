<!DOCTYPE html>
<html class="no-js" lang="uk-UA">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Підтримка | Acer Україна</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{asset('acer/icon" rel="stylesheet')}}">
    <meta name="twitter:card" content="support">
    <meta property="og:type" content="support">
    <meta name="breadcrumb" content="Yes">
    <meta name="searchType" content="Підтримка">
    <meta name="leftNavigationSection" content="Support|2859">
    <meta property="og:title" content="Підтримка | Acer Україна">
    <meta property="og:locale" content="uk_ua">
    <meta name="description"
          content="Отримуйте обслуговування та підтримку виробів Acer, шукайте оновлення BIOS, посібники, драйвери та виправлення.">
    <meta name="pageBrand" content="Acer">
    <meta name="searchCategory" content="Підтримка">
    <meta property="og:url" content="https://www.acer.com/ua-uk/support">
    <meta property="og:description"
          content="Отримуйте обслуговування та підтримку виробів Acer, шукайте оновлення BIOS, посібники, драйвери та виправлення.">
    <meta property="og:site_name" content="Acer Україна">
    <meta name="twitter:title" content="Підтримка | Acer Україна">
    <meta name="twitter:description"
          content="Отримуйте обслуговування та підтримку виробів Acer, шукайте оновлення BIOS, посібники, драйвери та виправлення.">
    <meta name="twitter:url" content="https://www.acer.com/ua-uk/support">
    <meta name="twitter:site" content="Acer Україна">
    <meta name="robots" content="max-image-preview:large">
    <link rel="canonical" href="https://www.acer.com/ua-uk/support">


    <link rel="icon" type="image/x-icon" href="{{asset("favicon.ico")}}">
    <link rel="preconnect" href="https://fonts.gstatic.com/">

    <link rel="stylesheet" href="{{asset('acer/css2')}}">
    <link rel="stylesheet" href="{{asset('acer/main.css')}}">
    <link rel="stylesheet" href="{{asset('acer/print.css')}}">

    <script src="{{asset('acer/objectFitPolyfill.min.js')}}"></script>
    <script src="{{asset('acer/ofi.min.js')}}"
            integrity="sha512-7taFZYSf0eAWyi1UvMzNrBoPVuvLU7KX6h10e4AzyHVnPjzuxeGWbXYX+ED9zXVVq+r9Xox5WqvABACBSCevmg=="
            crossorigin="anonymous"></script>
    <script src="{{asset('acer/polyfill.min.js')}}"></script>
    <script>const vids = [];</script>

    <script src="{{asset('acer/api.js')}}" async="" defer=""></script>

    <script src="{{asset('acer/jquery-3.6.0.min.js')}}"></script>

    <script>
        var svg_url = 'https://static.acer.com/system/assets/images/icons.svg';
        var ajax;
        ajax = new XMLHttpRequest();
        ajax.open('GET', svg_url, true);
        ajax.onload = function (e) {
            var div = document.createElement('div');
            div.innerHTML = ajax.responseText;
            document.body.insertBefore(div, document.body.childNodes[0]);
        }
        ajax.send();
    </script>

    <script type="text/javascript" src="{{asset('acer/37f82fc8')}}" defer=""></script>
    <link rel="stylesheet" href="{{asset('acer/jquery-ui.css')}}">
    @yield('stack-scripts')
</head>

<body class="acer" data-all-brands="ConceptD,Predator,Acer">
<div>
    <svg xmlns="http://www.w3.org/2000/svg" width="0" height="0" class="icon-sprite">
        <symbol viewBox="0 0 480 480" id="facebook">
            <path
                d="M272.442 440V257.542h61.918l9.268-71.105h-71.186v-45.4c0-20.584 5.782-34.616 35.625-34.616l38.068-.02V42.8c-6.583-.864-29.18-2.8-55.472-2.8-54.882 0-92.456 33.136-92.456 93.993v52.44h-62.072v71.105h62.072v182.458h74.235z"></path>
        </symbol>
        <symbol viewBox="0 0 24 24" id="google-plus">
            <g transform="matrix(1, 0, 0, 1, 27.009001, -39.238998)">
                <path fill="#4285F4"
                      d="M -3.264 51.509 C -3.264 50.719 -3.334 49.969 -3.454 49.239 L -14.754 49.239 L -14.754 53.749 L -8.284 53.749 C -8.574 55.229 -9.424 56.479 -10.684 57.329 L -10.684 60.329 L -6.824 60.329 C -4.564 58.239 -3.264 55.159 -3.264 51.509 Z"></path>
                <path fill="#34A853"
                      d="M -14.754 63.239 C -11.514 63.239 -8.804 62.159 -6.824 60.329 L -10.684 57.329 C -11.764 58.049 -13.134 58.489 -14.754 58.489 C -17.884 58.489 -20.534 56.379 -21.484 53.529 L -25.464 53.529 L -25.464 56.619 C -23.494 60.539 -19.444 63.239 -14.754 63.239 Z"></path>
                <path fill="#FBBC05"
                      d="M -21.484 53.529 C -21.734 52.809 -21.864 52.039 -21.864 51.239 C -21.864 50.439 -21.724 49.669 -21.484 48.949 L -21.484 45.859 L -25.464 45.859 C -26.284 47.479 -26.754 49.299 -26.754 51.239 C -26.754 53.179 -26.284 54.999 -25.464 56.619 L -21.484 53.529 Z"></path>
                <path fill="#EA4335"
                      d="M -14.754 43.989 C -12.984 43.989 -11.404 44.599 -10.154 45.789 L -6.734 42.369 C -8.804 40.429 -11.514 39.239 -14.754 39.239 C -19.444 39.239 -23.494 41.939 -25.464 45.859 L -21.484 48.949 C -20.534 46.099 -17.884 43.989 -14.754 43.989 Z"></path>
            </g>
        </symbol>
        <symbol viewBox="0 0 16 16" id="google">
            <path
                d="M15.545 6.558a9.42 9.42 0 0 1 .139 1.626c0 2.434-.87 4.492-2.384 5.885h.002C11.978 15.292 10.158 16 8 16A8 8 0 1 1 8 0a7.689 7.689 0 0 1 5.352 2.082l-2.284 2.284A4.347 4.347 0 0 0 8 3.166c-2.087 0-3.86 1.408-4.492 3.304a4.792 4.792 0 0 0 0 3.063h.003c.635 1.893 2.405 3.301 4.492 3.301 1.078 0 2.004-.276 2.722-.764h-.003a3.702 3.702 0 0 0 1.599-2.431H8v-3.08h7.545z"
                fill="white"></path>
        </symbol>
        <symbol viewBox="0 0 480 480" id="twitter">
            <path
                d="M399.041 158.44c.151 3.519.248 7.054.248 10.613 0 108.422-82.55 233.447-233.491 233.447-46.343 0-89.48-13.596-125.798-36.871a168.185 168.185 0 0019.575 1.152c38.447 0 73.831-13.117 101.919-35.127-35.92-.657-66.215-24.388-76.663-56.987a81.26 81.26 0 0015.448 1.48c7.48 0 14.736-1.008 21.615-2.88-37.543-7.533-65.83-40.693-65.83-80.443 0-.353 0-.688.007-1.033a81.956 81.956 0 0037.168 10.263c-22.014-14.717-36.502-39.823-36.502-68.295 0-15.029 4.048-29.13 11.112-41.246 40.478 49.643 100.942 82.3 169.148 85.73a82.362 82.362 0 01-2.12-18.7c0-45.308 36.735-82.043 82.063-82.043 23.607 0 44.935 9.957 59.902 25.905a164.229 164.229 0 0052.103-19.906c-6.12 19.162-19.135 35.231-36.079 45.387 16.591-1.982 32.415-6.389 47.135-12.916-11.016 16.46-24.92 30.912-40.951 42.47z"></path>
        </symbol>
        <symbol viewBox="0 0 480 480" id="youtube">
            <path
                d="M250.4,401.4l-96-1.8c-31.1-0.6-62.2,0.6-92.7-5.7c-46.3-9.5-49.6-55.9-53.1-94.8c-4.7-54.7-2.9-110.5,6-164.7 c5-30.5,24.9-48.6,55.6-50.6c103.6-7.2,207.9-6.3,311.3-3c10.9,0.3,21.9,2,32.7,3.9c53.1,9.3,54.4,61.9,57.9,106.2 c3.4,44.7,2,89.7-4.6,134.1c-5.3,36.8-15.3,67.6-57.9,70.6c-53.3,3.9-105.4,7-158.8,6C250.8,401.4,250.5,401.4,250.4,401.4zM193.9,308.2c40.2-23.1,79.5-45.7,119.5-68.6c-40.2-23.1-79.5-45.7-119.5-68.6V308.2z"></path>
        </symbol>
        <symbol viewBox="0 5 1036 990" id="linkedin">
            <path
                d="M0 120c0-33.334 11.667-60.834 35-82.5C58.333 15.833 88.667 5 126 5c36.667 0 66.333 10.666 89 32 23.333 22 35 50.666 35 86 0 32-11.333 58.666-34 80-23.333 22-54 33-92 33h-1c-36.667 0-66.333-11-89-33S0 153.333 0 120zm13 875V327h222v668H13zm345 0h222V622c0-23.334 2.667-41.334 8-54 9.333-22.667 23.5-41.834 42.5-57.5 19-15.667 42.833-23.5 71.5-23.5 74.667 0 112 50.333 112 151v357h222V612c0-98.667-23.333-173.5-70-224.5S857.667 311 781 311c-86 0-153 37-201 111v2h-1l1-2v-95H358c1.333 21.333 2 87.666 2 199 0 111.333-.667 267.666-2 469z"></path>
        </symbol>
        <symbol viewBox="0 0 774 1000.2" id="pinterest">
            <path
                d="M0 359c0-42 8.8-83.7 26.5-125s43-79.7 76-115 76.3-64 130-86S345.7 0 411 0c106 0 193 32.7 261 98s102 142.3 102 231c0 114-28.8 208.2-86.5 282.5S555.3 723 464 723c-30 0-58.2-7-84.5-21s-44.8-31-55.5-51l-40 158c-3.3 12.7-7.7 25.5-13 38.5S259.8 873 253.5 885c-6.3 12-12.7 23.3-19 34s-12.7 20.7-19 30-11.8 17.2-16.5 23.5-9 11.8-13 16.5l-6 8c-2 2.7-4.7 3.7-8 3s-5.3-2.7-6-6c0-.7-.5-5.3-1.5-14s-2-17.8-3-27.5-2-22.2-3-37.5-1.3-30.2-1-44.5 1.3-30.2 3-47.5 4.2-33.3 7.5-48c7.3-31.3 32-135.7 74-313-5.3-10.7-9.7-23.5-13-38.5s-5-27.2-5-36.5l-1-15c0-42.7 10.8-78.2 32.5-106.5S303.3 223 334 223c24.7 0 43.8 8.2 57.5 24.5S412 284.3 412 309c0 15.3-2.8 34.2-8.5 56.5s-13.2 48-22.5 77-16 52.5-20 70.5c-6.7 30-.8 56 17.5 78s42.8 33 73.5 33c52.7 0 96.2-29.8 130.5-89.5S634 402.7 634 318c0-64.7-21-117.5-63-158.5S470.3 98 395 98c-84 0-152.2 27-204.5 81S112 297.7 112 373c0 44.7 12.7 82.3 38 113 8.7 10 11.3 20.7 8 32-1.3 3.3-3.3 11-6 23s-4.7 19.7-6 23c-1.3 7.3-4.7 12.2-10 14.5s-11.3 2.2-18-.5c-39.3-16-68.8-43.5-88.5-82.5S0 411 0 359z"></path>
        </symbol>
        <symbol viewBox="0 0 50 50" id="wechat">
            <path class="st0" d="M34.7,17.6c-4.6,0.2-8.7,1.6-11.9,4.8c-3.3,3.2-4.8,7.1-4.4,12c-1.8-0.2-3.5-0.5-5.1-0.6c-0.6,0-1.3,0-1.7,0.3
      c-1.6,0.9-3.2,1.9-5,3.1c0.3-1.5,0.6-2.8,0.9-4.1c0.3-0.9,0.2-1.5-0.7-2.1c-5.6-3.9-7.9-9.8-6.2-15.9c1.6-5.6,5.6-9,11-10.8
      c7.4-2.4,15.7,0,20.2,5.9C33.5,12.4,34.5,14.8,34.7,17.6L34.7,17.6z M13.4,15.8c0-1.1-0.9-2.1-2.1-2.1c-1.2,0-2.1,0.9-2.2,2
      c0,1.2,0.9,2.1,2,2.1C12.4,17.8,13.4,16.9,13.4,15.8L13.4,15.8z M24.5,13.6c-1.1,0-2.1,1-2.1,2.1c0,1.2,1,2.1,2.1,2
      c1.2,0,2.1-0.9,2.1-2.1C26.6,14.5,25.7,13.6,24.5,13.6z"></path>
            <path class="st0" d="M45.1,46.5c-1.5-0.7-2.8-1.6-4.2-1.8c-1.4-0.1-2.9,0.7-4.4,0.8c-4.5,0.5-8.6-0.8-12-3.9
      c-6.4-5.9-5.5-15,1.9-19.8c6.6-4.3,16.2-2.9,20.8,3.1c4,5.2,3.6,12.1-1.4,16.5c-1.4,1.3-1.9,2.3-1,4C45,45.7,45,46.1,45.1,46.5
      L45.1,46.5z M28.4,30.3c0.9,0,1.7-0.7,1.7-1.7c0-1-0.7-1.8-1.7-1.8c-1,0-1.8,0.8-1.7,1.8C26.7,29.6,27.5,30.3,28.4,30.3L28.4,30.3z
      M39.2,26.9c-0.9,0-1.7,0.7-1.7,1.7c0,1,0.7,1.8,1.7,1.8c0.9,0,1.7-0.7,1.7-1.6C40.9,27.7,40.2,26.9,39.2,26.9z"></path>
        </symbol>
        <symbol viewBox="0 0 16 16" id="TikTok">
            <path
                d="M9 0h1.98c.144.715.54 1.617 1.235 2.512C12.895 3.389 13.797 4 15 4v2c-1.753 0-3.07-.814-4-1.829V11a5 5 0 1 1-5-5v2a3 3 0 1 0 3 3V0Z"
                fill="white"></path>
        </symbol>
        <symbol viewBox="0 0 16 16" id="instagram">
            <path
                d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"
                fill="white"></path>
        </symbol>
        <symbol viewBox="0 0 16 16" id="vimeo">
            <path
                d="M15.992 4.204c-.071 1.556-1.158 3.687-3.262 6.393-2.175 2.829-4.016 4.243-5.522 4.243-.933 0-1.722-.861-2.367-2.583L3.55 7.523C3.07 5.8 2.556 4.94 2.007 4.94c-.118 0-.537.253-1.254.754L0 4.724a209.56 209.56 0 0 0 2.334-2.081c1.054-.91 1.845-1.388 2.373-1.437 1.243-.123 2.01.728 2.298 2.553.31 1.968.526 3.19.646 3.666.36 1.631.756 2.446 1.186 2.445.334 0 .836-.53 1.508-1.587.671-1.058 1.03-1.863 1.077-2.415.096-.913-.263-1.37-1.077-1.37a3.022 3.022 0 0 0-1.185.261c.789-2.573 2.291-3.825 4.508-3.756 1.644.05 2.419 1.117 2.324 3.2z"
                fill="white"></path>
        </symbol>
        <symbol viewBox="0 0 16 16" id="behance">
            <path
                d="M4.654 3c.461 0 .887.035 1.278.14.39.07.711.216.996.391.286.176.497.426.641.747.14.32.216.711.216 1.137 0 .496-.106.922-.356 1.242-.215.32-.566.606-.997.817.606.176 1.067.496 1.348.922.281.426.461.957.461 1.563 0 .496-.105.922-.285 1.278a2.317 2.317 0 0 1-.782.887c-.32.215-.711.39-1.137.496a5.329 5.329 0 0 1-1.278.176L0 12.803V3h4.654zm-.285 3.978c.39 0 .71-.105.957-.285.246-.18.355-.497.355-.887 0-.216-.035-.426-.105-.567a.981.981 0 0 0-.32-.355 1.84 1.84 0 0 0-.461-.176c-.176-.035-.356-.035-.567-.035H2.17v2.31c0-.005 2.2-.005 2.2-.005zm.105 4.193c.215 0 .426-.035.606-.07.176-.035.356-.106.496-.216s.25-.215.356-.39c.07-.176.14-.391.14-.641 0-.496-.14-.852-.426-1.102-.285-.215-.676-.32-1.137-.32H2.17v2.734h2.305v.005zm6.858-.035c.286.285.711.426 1.278.426.39 0 .746-.106 1.032-.286.285-.215.46-.426.53-.64h1.74c-.286.851-.712 1.457-1.278 1.848-.566.355-1.243.566-2.06.566a4.135 4.135 0 0 1-1.527-.285 2.827 2.827 0 0 1-1.137-.782 2.851 2.851 0 0 1-.712-1.172c-.175-.461-.25-.957-.25-1.528 0-.531.07-1.032.25-1.493.18-.46.426-.852.747-1.207.32-.32.711-.606 1.137-.782a4.018 4.018 0 0 1 1.493-.285c.606 0 1.137.105 1.598.355.46.25.817.532 1.102.958.285.39.496.851.641 1.348.07.496.105.996.07 1.563h-5.15c0 .58.21 1.11.496 1.396zm2.24-3.732c-.25-.25-.642-.391-1.103-.391-.32 0-.566.07-.781.176-.215.105-.356.25-.496.39a.957.957 0 0 0-.25.497c-.036.175-.07.32-.07.46h3.196c-.07-.526-.25-.882-.497-1.132zm-3.127-3.728h3.978v.957h-3.978v-.957z"
                fill="white"></path>
        </symbol>
        <symbol viewBox="0 0 16 16" id="website">
            <path
                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm7.5-6.923c-.67.204-1.335.82-1.887 1.855A7.97 7.97 0 0 0 5.145 4H7.5V1.077zM4.09 4a9.267 9.267 0 0 1 .64-1.539 6.7 6.7 0 0 1 .597-.933A7.025 7.025 0 0 0 2.255 4H4.09zm-.582 3.5c.03-.877.138-1.718.312-2.5H1.674a6.958 6.958 0 0 0-.656 2.5h2.49zM4.847 5a12.5 12.5 0 0 0-.338 2.5H7.5V5H4.847zM8.5 5v2.5h2.99a12.495 12.495 0 0 0-.337-2.5H8.5zM4.51 8.5a12.5 12.5 0 0 0 .337 2.5H7.5V8.5H4.51zm3.99 0V11h2.653c.187-.765.306-1.608.338-2.5H8.5zM5.145 12c.138.386.295.744.468 1.068.552 1.035 1.218 1.65 1.887 1.855V12H5.145zm.182 2.472a6.696 6.696 0 0 1-.597-.933A9.268 9.268 0 0 1 4.09 12H2.255a7.024 7.024 0 0 0 3.072 2.472zM3.82 11a13.652 13.652 0 0 1-.312-2.5h-2.49c.062.89.291 1.733.656 2.5H3.82zm6.853 3.472A7.024 7.024 0 0 0 13.745 12H11.91a9.27 9.27 0 0 1-.64 1.539 6.688 6.688 0 0 1-.597.933zM8.5 12v2.923c.67-.204 1.335-.82 1.887-1.855.173-.324.33-.682.468-1.068H8.5zm3.68-1h2.146c.365-.767.594-1.61.656-2.5h-2.49a13.65 13.65 0 0 1-.312 2.5zm2.802-3.5a6.959 6.959 0 0 0-.656-2.5H12.18c.174.782.282 1.623.312 2.5h2.49zM11.27 2.461c.247.464.462.98.64 1.539h1.835a7.024 7.024 0 0 0-3.072-2.472c.218.284.418.598.597.933zM10.855 4a7.966 7.966 0 0 0-.468-1.068C9.835 1.897 9.17 1.282 8.5 1.077V4h2.355z"
                fill="white"></path>
        </symbol>
        <symbol viewBox="0 0 24 24" id="vk">
            <path
                d="M23.4493 5.94799C23.6161 5.40154 23.4493 5 22.6553 5H20.0297C19.3621 5 19.0543 5.34687 18.8874 5.72936C18.8874 5.72936 17.5521 8.92607 15.6606 11.0025C15.0487 11.6036 14.7705 11.7949 14.4367 11.7949C14.2698 11.7949 14.0194 11.6036 14.0194 11.0572V5.94799C14.0194 5.29225 13.8345 5 13.2781 5H9.15213C8.73494 5 8.48403 5.30434 8.48403 5.59278C8.48403 6.21441 9.42974 6.35777 9.52722 8.10641V11.9042C9.52722 12.7368 9.37413 12.8878 9.04032 12.8878C8.15023 12.8878 5.98507 9.67682 4.70093 6.00261C4.44927 5.28847 4.19686 5 3.52583 5H0.900218C0.150044 5 0 5.34687 0 5.72936C0 6.41244 0.890141 9.80039 4.14464 14.2812C6.31429 17.3412 9.37118 19 12.1528 19C13.8218 19 14.0283 18.6316 14.0283 17.997V15.6842C14.0283 14.9474 14.1864 14.8003 14.7149 14.8003C15.1043 14.8003 15.7719 14.9916 17.3296 16.467C19.1099 18.2156 19.4034 19 20.4047 19H23.0304C23.7805 19 24.1556 18.6316 23.9392 17.9045C23.7024 17.1799 22.8525 16.1286 21.7247 14.8823C21.1127 14.1719 20.1947 13.4069 19.9165 13.0243C19.5271 12.5326 19.6384 12.314 19.9165 11.8769C19.9165 11.8769 23.1155 7.45067 23.4493 5.94799Z"
                fill="white"></path>
        </symbol>
        <symbol viewBox="0 0 16 16" id="Discord">
            <path
                d="M13.545 2.907a13.227 13.227 0 0 0-3.257-1.011.05.05 0 0 0-.052.025c-.141.25-.297.577-.406.833a12.19 12.19 0 0 0-3.658 0 8.258 8.258 0 0 0-.412-.833.051.051 0 0 0-.052-.025c-1.125.194-2.22.534-3.257 1.011a.041.041 0 0 0-.021.018C.356 6.024-.213 9.047.066 12.032c.001.014.01.028.021.037a13.276 13.276 0 0 0 3.995 2.02.05.05 0 0 0 .056-.019c.308-.42.582-.863.818-1.329a.05.05 0 0 0-.01-.059.051.051 0 0 0-.018-.011 8.875 8.875 0 0 1-1.248-.595.05.05 0 0 1-.02-.066.051.051 0 0 1 .015-.019c.084-.063.168-.129.248-.195a.05.05 0 0 1 .051-.007c2.619 1.196 5.454 1.196 8.041 0a.052.052 0 0 1 .053.007c.08.066.164.132.248.195a.051.051 0 0 1-.004.085 8.254 8.254 0 0 1-1.249.594.05.05 0 0 0-.03.03.052.052 0 0 0 .003.041c.24.465.515.909.817 1.329a.05.05 0 0 0 .056.019 13.235 13.235 0 0 0 4.001-2.02.049.049 0 0 0 .021-.037c.334-3.451-.559-6.449-2.366-9.106a.034.034 0 0 0-.02-.019Zm-8.198 7.307c-.789 0-1.438-.724-1.438-1.612 0-.889.637-1.613 1.438-1.613.807 0 1.45.73 1.438 1.613 0 .888-.637 1.612-1.438 1.612Zm5.316 0c-.788 0-1.438-.724-1.438-1.612 0-.889.637-1.613 1.438-1.613.807 0 1.451.73 1.438 1.613 0 .888-.631 1.612-1.438 1.612Z"
                fill="white"></path>
        </symbol>
        <symbol viewBox="0 0 16 16" id="Twitch">
            <path
                d="M3.857 0 1 2.857v10.286h3.429V16l2.857-2.857H9.57L14.714 8V0H3.857zm9.714 7.429-2.285 2.285H9l-2 2v-2H4.429V1.143h9.142v6.286z"
                fill="white"></path>
            <path d="M11.857 3.143h-1.143V6.57h1.143V3.143zm-3.143 0H7.571V6.57h1.143V3.143z" fill="white"></path>
        </symbol>
        <symbol viewBox="0 0 24 24" id="weibo">
            <path fill-rule="nonzero"
                  d="M17.525 11.378c1.263.392 2.669 1.336 2.669 3.004 0 2.763-3.98 6.239-9.964 6.239-4.565 0-9.23-2.213-9.23-5.852 0-1.902 1.204-4.102 3.277-6.177 2.773-2.77 6.004-4.033 7.219-2.816.537.537.588 1.464.244 2.572-.178.557.525.25.525.25 2.24-.938 4.196-.994 4.909.027.38.543.343 1.306-.008 2.19-.163.407.048.471.36.563zm-7.282 7.939c3.641-.362 6.401-2.592 6.167-4.983-.237-2.391-3.382-4.038-7.023-3.677-3.64.36-6.403 2.59-6.167 4.98.237 2.394 3.382 4.039 7.023 3.68zM6.16 14.438c.754-1.527 2.712-2.39 4.446-1.94 1.793.463 2.707 2.154 1.976 3.8-.744 1.682-2.882 2.578-4.695 1.993-1.752-.566-2.493-2.294-1.727-3.853zm1.446 2.587c.568.257 1.325.013 1.676-.55.346-.568.163-1.217-.407-1.459-.563-.237-1.291.008-1.64.553-.354.547-.189 1.202.371 1.456zm2.206-1.808c.219.092.501-.012.628-.231.123-.22.044-.466-.178-.548-.216-.084-.486.018-.613.232-.123.214-.054.458.163.547zM19.873 9.5a.725.725 0 1 1-1.378-.451 1.38 1.38 0 0 0-.288-1.357 1.395 1.395 0 0 0-1.321-.425.723.723 0 1 1-.303-1.416 2.836 2.836 0 0 1 3.29 3.649zm-3.916-6.575A5.831 5.831 0 0 1 21.5 4.72a5.836 5.836 0 0 1 1.22 5.704.838.838 0 0 1-1.06.54.844.844 0 0 1-.542-1.062 4.143 4.143 0 0 0-4.807-5.327.845.845 0 0 1-.354-1.65z"
                  fill="white"></path>
        </symbol>
        <symbol viewBox="0 0 24 24" id="wechatFill">
            <path
                d="M18.574 13.711a.91.91 0 0 0 .898-.898c0-.498-.399-.898-.898-.898s-.898.4-.898.898c0 .5.4.898.898.898zm-4.425 0a.91.91 0 0 0 .898-.898c0-.498-.4-.898-.898-.898-.5 0-.898.4-.898.898 0 .5.399.898.898.898zm6.567 5.04a.347.347 0 0 0-.172.37c0 .048 0 .097.025.147.098.417.294 1.081.294 1.106 0 .073.025.122.025.172a.22.22 0 0 1-.221.22c-.05 0-.074-.024-.123-.048l-1.449-.836a.799.799 0 0 0-.344-.098c-.073 0-.147 0-.196.024-.688.197-1.4.295-2.161.295-3.66 0-6.607-2.457-6.607-5.505 0-3.047 2.947-5.505 6.607-5.505 3.659 0 6.606 2.458 6.606 5.505 0 1.647-.884 3.146-2.284 4.154zM16.673 8.099a9.105 9.105 0 0 0-.28-.005c-4.174 0-7.606 2.86-7.606 6.505 0 .554.08 1.09.228 1.6h-.089a9.963 9.963 0 0 1-2.584-.368c-.074-.025-.148-.025-.222-.025a.832.832 0 0 0-.418.123l-1.748 1.005c-.05.025-.099.05-.148.05a.273.273 0 0 1-.27-.27c0-.074.024-.123.049-.197.024-.024.246-.834.369-1.324 0-.05.024-.123.024-.172a.556.556 0 0 0-.221-.442C2.058 13.376 1 11.586 1 9.598 1 5.945 4.57 3 8.95 3c3.765 0 6.93 2.169 7.723 5.098zm-5.154.418c.573 0 1.026-.477 1.026-1.026 0-.573-.453-1.026-1.026-1.026s-1.026.453-1.026 1.026.453 1.026 1.026 1.026zm-5.26 0c.573 0 1.027-.477 1.027-1.026 0-.573-.454-1.026-1.027-1.026-.572 0-1.026.453-1.026 1.026s.454 1.026 1.026 1.026z"
                fill="white"></path>
        </symbol>
        <symbol viewBox="-1 2 26.5 26.5" id="apple">
            <path
                d="M13.313 7.219c0.25-0.688 0.406-1.281 0.406-1.844 0-0.094-0.031-0.156-0.031-0.25s-0.031-0.219-0.063-0.344c-1.563 0.344-2.656 1.031-3.344 2.031-0.688 0.969-1.031 2.125-1.063 3.469 0.563-0.063 1.031-0.156 1.375-0.25 0.469-0.156 0.969-0.469 1.469-0.938 0.563-0.563 1-1.25 1.25-1.875zM11.531 10.344c-0.938 0.313-1.594 0.438-1.938 0.438-0.281 0-0.875-0.125-1.844-0.375-0.938-0.281-1.719-0.438-2.406-0.438-1.531 0-2.813 0.656-3.844 1.969-1 1.313-1.5 2.969-1.5 5.031 0 2.219 0.656 4.469 1.969 6.781 1.344 2.313 2.719 3.438 4.063 3.438 0.438 0 1-0.156 1.781-0.438 0.719-0.281 1.344-0.438 1.906-0.438s1.25 0.125 2.031 0.406c0.813 0.281 1.438 0.406 1.906 0.406 1.156 0 2.313-0.875 3.469-2.594 0.375-0.563 0.719-1.156 1-1.719 0.25-0.531 0.469-1.094 0.656-1.625-0.844-0.25-1.563-0.844-2.156-1.719-0.594-0.906-0.906-1.906-0.906-3 0-1.031 0.281-1.938 0.844-2.781 0.344-0.438 0.875-1 1.563-1.594-0.219-0.281-0.469-0.563-0.688-0.781-0.25-0.219-0.469-0.406-0.688-0.563-0.875-0.563-1.844-0.875-2.906-0.875-0.625 0-1.406 0.188-2.313 0.469z"></path>
        </symbol>
    </svg>

</div>
<noscript>
    <style>
        [data-simplebar] {
            overflow: auto;
        }
    </style>
</noscript>



<header class="header">
    <div class="nav-local__inner">
        <div class="logo">
            <a class="logo__link"
               href="https://www.acer.com/ua-uk">
                <img class="logo__img" src="{{asset('acer/Acer-logo.svg')}}" alt="Acer-logo">
            </a>
        </div>
    </div>


{{--    <nav class="nav-global" data-ui-component="nav-global" role="navigation" aria-label="Global Navigation">--}}

{{--        <ul class="menu--global" property="brandNav">--}}
{{--            <li class="menu--global__item"><a class="menu--global__link"--}}
{{--                                              href="https://www.acer.com/ua-uk" target="_self" property="linkText"--}}
{{--                                              rel="nofollow">Acer</a></li>--}}
{{--            <li class="menu--global__item"><a class="menu--global__link"--}}
{{--                                              href="https://www.acer.com/ua-uk/predator" target="_blank"--}}
{{--                                              property="linkText" rel="nofollow">Predator</a></li>--}}
{{--            <li class="menu--global__item"><a class="menu--global__link"--}}
{{--                                              href="https://www.acer.com/ua-uk/conceptd" target="_blank"--}}
{{--                                              property="linkText" rel="nofollow">ConceptD</a></li>--}}
{{--            <li class="menu--global__item"><a class="menu--global__link"--}}
{{--                                              href="https://www.planet9.gg/" target="_blank" property="linkText"--}}
{{--                                              rel="nofollow">Planet9</a></li>--}}
{{--            <li class="menu--global__item"><a class="menu--global__link"--}}
{{--                                              href="https://www.acer.com/ua-uk/SpatialLabs" target="_blank"--}}
{{--                                              property="linkText" rel="nofollow">SpatialLabs</a></li>--}}
{{--        </ul>--}}


{{--        <ul class="menu--global" property="globalNav">--}}
{{--            <li class="menu--global__item">--}}
{{--                <a class="menu--global__link"--}}
{{--                   href="https://www.acer.com/ua-uk/languages">--}}
{{--                        <span class="material-icons menu--global__icon">--}}


{{--                                <img src="{{asset('acer/language.svg')}}" alt="Мова">--}}
{{--                        </span>--}}
{{--                    Мова--}}
{{--                </a>--}}
{{--            </li>--}}

{{--            <li style="display:none" class="menu--global__item menu--dropdown menu--account menu--account-login">--}}
{{--                <a class="menu--global__link" href="https://www.acer.com/ua-uk/account/my-profile">--}}

{{--                    <img src="{{asset('acer/account_circle.svg')}}" alt="account_circle">--}}
{{--                    <span class="user-name"></span>--}}
{{--                </a>--}}
{{--                <ul class="menu--dropdown__list">--}}
{{--                    <li class="menu--dropdown__item">--}}
{{--                        <a class="menu--dropdown__link"--}}
{{--                           href="https://www.acer.com/ua-uk/account/my-profile">--}}
{{--                            <span class="material-icons menu--global__icon">account_circle</span>--}}
{{--                            Мій профіль--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="menu--dropdown__item">--}}
{{--                        <a class="menu--dropdown__link"--}}
{{--                           href="https://www.acer.com/ua-uk/account/my-products">--}}
{{--                            <span class="material-icons menu--global__icon">phonelink</span>--}}
{{--                            Мої вироби--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="menu--dropdown__item">--}}
{{--                        <a class="menu--dropdown__link"--}}
{{--                           href="https://www.acer.com/ua-uk/account/register-product">--}}
{{--                            <span class="material-icons menu--global__icon">assignment_turned_in</span>--}}
{{--                            Зареєструйте виріб--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="menu--dropdown__item">--}}
{{--                        <a class="menu--dropdown__link"--}}
{{--                           href="https://community.acer.com/">--}}
{{--                            <span class="material-icons menu--global__icon">group</span>--}}
{{--                            Моя спільнота--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="menu--dropdown__item">--}}
{{--                        <a class="menu--dropdown__link"--}}
{{--                           href="https://www.acer.com/ua-uk/account/logout">--}}
{{--                            <span class="material-icons menu--global__icon">input</span>--}}
{{--                            Вийти--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            <li style="" class="menu--global__item menu--dropdown menu--account-not-login">--}}

{{--                <a class="menu--global__link"--}}
{{--                   href="#" rel="nofollow">--}}
{{--                    <span class="material-icons menu--global__icon">--}}
{{--                            <img src="{{asset('acer/account_circle.svg')}}" alt="account_circle">--}}
{{--                    </span>--}}
{{--                    Acer ID--}}
{{--                </a>--}}
{{--                <ul class="menu--dropdown__list">--}}

{{--                    <li class="menu--dropdown__item">--}}
{{--                        <a class="menu--dropdown__link"--}}
{{--                           href="https://www.acer.com/ua-uk/account/sign-in">--}}
{{--                            <span class="material-icons menu--global__icon">account_circle</span>--}}
{{--                            Увійти--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="menu--dropdown__item">--}}
{{--                        <a class="menu--dropdown__link"--}}
{{--                           href="https://www.acer.com/ua-uk/account/sign-up">--}}
{{--                            <span class="material-icons menu--global__icon">person_add</span>--}}
{{--                            Зареєструватися--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                    <li class="menu--dropdown__item">--}}
{{--                        <a class="menu--dropdown__link"--}}
{{--                           href="https://www.acer.com/ua-uk/whats-acer-id">--}}
{{--                            <span class="material-icons menu--global__icon"> help</span>--}}
{{--                            Що таке Acer ID?--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

{{--            <!-- add ecommerce check here -->--}}
{{--        </ul>--}}

{{--    </nav>--}}
{{--    <nav class="nav-local" data-ui-component="nav-local" role="navigation" aria-label="Main Navigation">--}}
{{--        <div class="nav-local__inner">--}}

{{--            <a--}}
{{--                class="mobile-menu-btn slideout-trigger" role="button" aria-label="menu" aria-expanded="false"--}}
{{--                data-open="panel" data-target="#nav-mobile">--}}
{{--                <div class="hb hb--icon4">--}}
{{--                    <span></span>--}}
{{--                    <span></span>--}}
{{--                    <span></span>--}}
{{--                </div>--}}
{{--            </a>--}}




{{--            <ul class="menu--local">--}}
{{--                <li class="menu--local__item">--}}
{{--                    <a target="" class="menu--local__link"--}}
{{--                       id="products" href="#menu2622" data-toggle="sub-menu">--}}
{{--                        Товари--}}
{{--                        <svg class="material-icons menu--local__icon" xmlns="http://www.w3.org/2000/svg" height="24"--}}
{{--                             width="24">--}}
{{--                            <path d="m12 15.4-6-6L7.4 8l4.6 4.6L16.6 8 18 9.4Z"></path>--}}
{{--                        </svg>--}}

{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu--local__item">--}}
{{--                    <a target="" class="menu--local__link"--}}
{{--                       id="products" href="https://www.acer.com/ua-uk/business" data-toggle="sub-menu">--}}
{{--                        Для бізнесу--}}

{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu--local__item">--}}
{{--                    <a target="" class="menu--local__link"--}}
{{--                       id="products" href="https://www.acer.com/ua-uk/education" data-toggle="sub-menu" rel="nofollow">--}}
{{--                        Для освіти--}}

{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu--local__item">--}}
{{--                    <a target="" class="menu--local__link"--}}
{{--                       id="products" href="#menu2623" data-toggle="sub-menu">--}}
{{--                        Рішення--}}
{{--                        <svg class="material-icons menu--local__icon" xmlns="http://www.w3.org/2000/svg" height="24"--}}
{{--                             width="24">--}}
{{--                            <path d="m12 15.4-6-6L7.4 8l4.6 4.6L16.6 8 18 9.4Z"></path>--}}
{{--                        </svg>--}}

{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu--local__item">--}}
{{--                    <a target="" class="menu--local__link"--}}
{{--                       id="products" href="https://www.acer.com/ua-uk/support" data-toggle="sub-menu">--}}
{{--                        Підтримка--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu--local__item">--}}
{{--                    <a target="" class="menu--local__link"--}}
{{--                       id="products" href="https://www.acer.com/ua-uk/events" data-toggle="sub-menu">--}}
{{--                        Події--}}

{{--                    </a>--}}
{{--                </li>--}}
{{--            </ul>--}}


{{--            <div class="search" role="search">--}}
{{--                <div class="search-widget" data-source="https://microservices.agw.acer.com/api-search/">--}}
{{--                    <div class="search-container">--}}
{{--                        <button type="button" class="search-toggle">--}}

{{--                            <img src="{{asset('acer/search.svg')}}" alt="search">--}}
{{--                        </button>--}}
{{--                        <div class="entry">--}}
{{--                            <form class="search-form" onsubmit="window.event.preventDefault();">--}}
{{--                                <input type="text" class="search-input ui-autocomplete-input" name="search"--}}
{{--                                       autocomplete="off" placeholder="Пошук"--}}
{{--                                       data-search-prod="/ua-uk/search/search-products" maxlength="50">--}}
{{--                            </form>--}}
{{--                            <button type="button" class="search-button">--}}
{{--                                <span class="material-icons">close</span>--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                        <ul class="search-suggestions"></ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--        <div class="menu-mega" id="menu2622">--}}
{{--            <ul class="menu-mega__list">--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/laptops">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/acer-swift-sf514-56-antimicrobial-finger-print-backlit-on-wallpaper-mist-green-01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/acer-swift-sf514-56-antimicrobial-finger-print-backlit-on-wallpaper-mist-green-01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/acer-swift-sf514-56-antimicrobial-finger-print-backlit-on-wallpaper-mist-green-01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/acer-swift-sf514-56-antimicrobial-finger-print-backlit-on-wallpaper-mist-green-01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/acer-swift-sf514-56-antimicrobial-finger-print-backlit-on-wallpaper-mist-green-01')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class=""--}}
{{--                                 src="{{asset('acer/acer-swift-sf514-56-antimicrobial-finger-print-backlit-on-wallpaper-mist-green-01')}}"--}}
{{--                                 alt="acer-swift-sf514-56-antimicrobial-finger-print-backlit-on-wallpaper-mist-green-01"--}}
{{--                                 title="" style="object-position:50% 50%"></picture>--}}
{{--                        Ноутбуки--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/desktops-and-all-in-ones">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/acer-aspire-c24-1751-wallpaper-black-01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/acer-aspire-c24-1751-wallpaper-black-01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/acer-aspire-c24-1751-wallpaper-black-01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/acer-aspire-c24-1751-wallpaper-black-01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/acer-aspire-c24-1751-wallpaper-black-01')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class=""--}}
{{--                                 src="{{asset('acer/acer-aspire-c24-1751-wallpaper-black-01')}}"--}}
{{--                                 alt="acer-aspire-c24-1751-wallpaper-black-01" title="" style="object-position:50% 50%">--}}
{{--                        </picture>--}}
{{--                        Настільні комп’ютери та моноблоки--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/tablets">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/ENDURO-Urban-T3-EUT310A-11A-wallpaper-UI-polaris-blue-01-7')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/ENDURO-Urban-T3-EUT310A-11A-wallpaper-UI-polaris-blue-01-7')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/ENDURO-Urban-T3-EUT310A-11A-wallpaper-UI-polaris-blue-01-7')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/ENDURO-Urban-T3-EUT310A-11A-wallpaper-UI-polaris-blue-01-7')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/ENDURO-Urban-T3-EUT310A-11A-wallpaper-UI-polaris-blue-01-7')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class=""--}}
{{--                                 src="{{asset('acer/ENDURO-Urban-T3-EUT310A-11A-wallpaper-UI-polaris-blue-01-7')}}"--}}
{{--                                 alt="ENDURO-Urban-T3-EUT310A-11A-wallpaper-UI-polaris-blue-01" title=""--}}
{{--                                 style="object-position:50% 50%"></picture>--}}
{{--                        Планшети--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/monitors">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/XV2_XV252QZ_wp_01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/XV2_XV252QZ_wp_01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/XV2_XV252QZ_wp_01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/XV2_XV252QZ_wp_01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/XV2_XV252QZ_wp_01')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class="" src="{{asset('acer/XV2_XV252QZ_wp_01')}}"--}}
{{--                                 alt="Nitro XV2 Product Image" title="" style="object-position:50% 50%"></picture>--}}
{{--                        Монітори--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/projectors">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/s1286h_s1386wh-02-light')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/s1286h_s1386wh-02-light')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/s1286h_s1386wh-02-light')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/s1286h_s1386wh-02-light')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/s1286h_s1386wh-02-light')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class="" src="{{asset('acer/s1286h_s1386wh-02-light')}}"--}}
{{--                                 alt="S1286H Product Images" title="" style="object-position:50% 50%"></picture>--}}
{{--                        Проєктори--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/digital-signage">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/Acer-Digital-Signage-DW550C-wallpaper-01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/Acer-Digital-Signage-DW550C-wallpaper-01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/Acer-Digital-Signage-DW550C-wallpaper-01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/Acer-Digital-Signage-DW550C-wallpaper-01')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/Acer-Digital-Signage-DW550C-wallpaper-01')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class=""--}}
{{--                                 src="{{asset('acer/Acer-Digital-Signage-DW550C-wallpaper-01')}}"--}}
{{--                                 alt="Acer-Digital-Signage-DW550C-wallpaper-01" title=""--}}
{{--                                 style="object-position:50% 50%"></picture>--}}
{{--                        Цифрові інформаційні панелі--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/accessories">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/acer-wireless-mouse-m501-wwcb-white_03')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/acer-wireless-mouse-m501-wwcb-white_03')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/acer-wireless-mouse-m501-wwcb-white_03')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/acer-wireless-mouse-m501-wwcb-white_03')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/acer-wireless-mouse-m501-wwcb-white_03')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class="" src="{{asset('acer/acer-wireless-mouse-m501-wwcb-white_03')}}"--}}
{{--                                 alt="acer-wireless-mouse-m501-wwcb-white_03" title="" style="object-position:50% 50%">--}}
{{--                        </picture>--}}
{{--                        Аксесуари--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/networking">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset("acer/acer-connect-vero-w6m-preview.webp")}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset("acer/acer-connect-vero-w6m-preview.webp")}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset("acer/acer-connect-vero-w6m-preview.webp")}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset("acer/acer-connect-vero-w6m-preview.webp")}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset("acer/acer-connect-vero-w6m-preview.webp")}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class=""--}}
{{--                                 src="{{asset("acer/acer-connect-vero-w6m-preview.webp")}}"--}}
{{--                                 alt="3D rendering of cyberpunk AI. Circuit board. Technology background. Central Computer Processors CPU and GPU concept. Motherboard digital chip. Tech science background"--}}
{{--                                 title="acer-connect-vero-w6m-preview" style="object-position:50% 50%"></picture>--}}
{{--                        Мережа--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/emobility">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/ebii-preview')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/ebii-preview')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/ebii-preview')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/ebii-preview')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/ebii-preview')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class="" src="{{asset('acer/ebii-preview')}}" alt="ebii-preview"--}}
{{--                                 title="ebii-preview" style="object-position:50% 50%"></picture>--}}
{{--                        Електромобільність--}}
{{--                    </a>--}}
{{--                </li>--}}

{{--            </ul>--}}
{{--        </div>--}}
{{--        <div class="menu-mega" id="menu2623">--}}
{{--            <ul class="menu-mega__list">--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/education">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/Solutions_00_Home_RemoteLearningSetup-large-1')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/Solutions_00_Home_RemoteLearningSetup-large-1')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/Solutions_00_Home_RemoteLearningSetup-large-1')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/Solutions_00_Home_RemoteLearningSetup-large-1')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/Solutions_00_Home_RemoteLearningSetup-large-1')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class=""--}}
{{--                                 src="{{asset('acer/Solutions_00_Home_RemoteLearningSetup-large-1')}}"--}}
{{--                                 alt="Solutions_00_Home_RemoteLearningSetup-large" title=""--}}
{{--                                 style="object-position:50% 50%"></picture>--}}
{{--                        Acer for Education--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://emea-acerforbusiness.acer.com/" rel="nofollow">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/travelmate-p6-ksp-03-ls')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/travelmate-p6-ksp-03-ls')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/travelmate-p6-ksp-03-ls')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/travelmate-p6-ksp-03-ls')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/travelmate-p6-ksp-03-ls')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class="" src="{{asset('acer/travelmate-p6-ksp-03-ls')}}"--}}
{{--                                 alt="Photographer Journalist Working Studio Agency  Concept; Shutterstock ID 428685844; Purchase Order: -"--}}
{{--                                 title="travelmate-p6-ksp-03-ls" style="object-position:50% 50%"></picture>--}}
{{--                        Acer for Business--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/travelmate-solutions">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/tmp6-landingpage-comprehensive-security')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/tmp6-landingpage-comprehensive-security')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/tmp6-landingpage-comprehensive-security')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/tmp6-landingpage-comprehensive-security')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/tmp6-landingpage-comprehensive-security')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class=""--}}
{{--                                 src="{{asset('acer/tmp6-landingpage-comprehensive-security')}}"--}}
{{--                                 alt="tmp6-landingpage-comprehensive-security" title="" style="object-position:50% 50%">--}}
{{--                        </picture>--}}
{{--                        TravelMate--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/antimicrobial">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/ksp2-CorningGorillaGlass')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/ksp2-CorningGorillaGlass')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/ksp2-CorningGorillaGlass')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/ksp2-CorningGorillaGlass')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/ksp2-CorningGorillaGlass')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class="" src="{{asset('acer/ksp2-CorningGorillaGlass')}}"--}}
{{--                                 alt="Antimicrobial Solutions AGW Source" title="" style="object-position:50% 50%">--}}
{{--                        </picture>--}}
{{--                        Антимікробні рішення--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/military-standards">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/Enduro')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/Enduro')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/Enduro')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/Enduro')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/Enduro')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class="" src="{{asset('acer/Enduro')}}"--}}
{{--                                 alt="Military Standards AGW Source" title="" style="object-position:50% 50%"></picture>--}}
{{--                        Армійські стандарти--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/windows-11">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/Windows11_AGW_Banner_1024-1')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/Windows11_AGW_Banner_1024-1')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/Windows11_AGW_Banner_1024-1')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/Windows11_AGW_Banner_1024-1')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/Windows11_AGW_Banner_1024-1')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class="" src="{{asset('acer/Windows11_AGW_Banner_1024-1')}}"--}}
{{--                                 alt="Windows 11" title="" style="object-position:50% 50%"></picture>--}}
{{--                        Windows 11--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="menu-mega__item">--}}
{{--                    <a class="menu-mega__link"--}}
{{--                       href="https://www.acer.com/ua-uk/chromeos-enterprise">--}}
{{--                        <picture class="menu-mega__image image" style="object-position:50% 50%">--}}
{{--                            <source media="(max-width: 375px)"--}}
{{--                                    srcset="{{asset('acer/TA-04')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 376px) and (max-width: 640px)"--}}
{{--                                    srcset="{{asset('acer/TA-04')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 641px) and (max-width: 899px)"--}}
{{--                                    srcset="{{asset('acer/TA-04')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 900px) and (max-width: 1366px)"--}}
{{--                                    srcset="{{asset('acer/TA-04')}}?$Mega-Menu-Icon-L-M1-M2-S$">--}}
{{--                            <source media="(min-width: 1367px)"--}}
{{--                                    srcset="{{asset('acer/TA-04')}}?$Mega-Menu-Icon-XL$">--}}
{{--                            <img class="" src="{{asset('acer/TA-04')}}" alt="TA-04" title=""--}}
{{--                                 style="object-position:50% 50%"></picture>--}}
{{--                        ChromeOS Enterprise--}}
{{--                    </a>--}}
{{--                </li>--}}

{{--            </ul>--}}
{{--        </div>--}}
{{--    </nav>--}}


{{--    <div class="slideout slideout--nav" id="nav-mobile">--}}
{{--        <div class="slideout__inner">--}}
{{--            <nav class="nav-mobile">--}}

{{--                <div class="nav-panel__section nav-panel--no-padding">--}}
{{--                    <div class="is-drilldown">--}}
{{--                        <div class="nav-mobile__menu">--}}
{{--                            <div style="display: none; height: 0px;" class="is-drilldown menu--account-mobile-login">--}}
{{--                                <ul class="menu--mobile drilldown" data-drilldown="">--}}
{{--                                    <li class="menu__item--mobile is-drilldown__submenu-parent nav-mobile__auth">--}}
{{--                                        <a class="menu__link--mobile user-name"--}}
{{--                                           href="https://www.acer.com/ua-uk/account/my-profile">waiting</a>--}}
{{--                                        <ul class="menu__submenu is-drilldown__submenu">--}}
{{--                                            <li class="menu__item--mobile js-drilldown-back"><a--}}
{{--                                                    href="#"--}}
{{--                                                    class="menu__link--mobile">waiting</a></li>--}}

{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/account/my-profile">--}}
{{--                                                    <span class="material-icons">account_circle</span>--}}
{{--                                                    <span>Мій профіль</span>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/account/my-products">--}}
{{--                                                    <span class="material-icons">phonelink</span>--}}
{{--                                                    <span>Мої вироби</span>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/account/register-product">--}}
{{--                                                    <span class="material-icons">assignment_turned_in</span>--}}
{{--                                                    <span>Зареєструйте виріб</span>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile" href="https://community.acer.com/">--}}
{{--                                                    <span class="material-icons">group</span>--}}
{{--                                                    <span>Моя спільнота</span>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/account/logout">--}}
{{--                                                    <span class="material-icons">input</span>--}}
{{--                                                    <span>Вийти</span>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                            <div style="height: auto;" class="is-drilldown menu--account-mobile-not-login">--}}
{{--                                <ul class="menu--mobile drilldown" data-drilldown="">--}}
{{--                                    <li class="menu__item--mobile">--}}
{{--                                        <a class="menu__link--mobile" href="https://www.acer.com/ua-uk/account/sign-in">--}}

{{--                                            <span>Увійти </span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li class="menu__item--mobile">--}}
{{--                                        <a class="menu__link--mobile" href="https://www.acer.com/ua-uk/account/sign-up">--}}

{{--                                            <span>Зареєструватися</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="nav-panel__section nav-panel--no-padding">--}}
{{--                    <div class="is-drilldown">--}}
{{--                        <div class="nav-mobile__menu">--}}
{{--                            <div class="is-drilldown">--}}
{{--                                <ul class="menu--mobile drilldown" data-drilldown="">--}}
{{--                                    <li class="menu__item--mobile is-drilldown__submenu-parent">--}}
{{--                                        <a class="menu__link--mobile" href="#">--}}
{{--                                            Товари--}}
{{--                                        </a>--}}
{{--                                        <ul class="menu__submenu is-drilldown__submenu">--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/laptops">--}}
{{--                                                    Ноутбуки--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/desktops-and-all-in-ones">--}}
{{--                                                    Настільні комп’ютери та моноблоки--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/tablets">--}}
{{--                                                    Планшети--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/monitors">--}}
{{--                                                    Монітори--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/projectors">--}}
{{--                                                    Проєктори--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/digital-signage">--}}
{{--                                                    Цифрові інформаційні панелі--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/accessories">--}}
{{--                                                    Аксесуари--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/emobility">--}}
{{--                                                    Електромобільність--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}
{{--                                    </li>--}}
{{--                                    <li class="menu__item--mobile"--}}
{{--                                    >--}}
{{--                                        <a class="menu__link--mobile" id="products"--}}
{{--                                           href="https://www.acer.com/ua-uk/business" data-toggle="sub-menu">--}}
{{--                                            Для бізнесу--}}

{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li class="menu__item--mobile"--}}
{{--                                    >--}}
{{--                                        <a class="menu__link--mobile" id="products"--}}
{{--                                           href="https://www.acer.com/ua-uk/education" data-toggle="sub-menu"--}}
{{--                                           rel="nofollow">--}}
{{--                                            Для освіти--}}

{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li class="menu__item--mobile is-drilldown__submenu-parent"--}}
{{--                                    >--}}
{{--                                        <a class="menu__link--mobile" href="https://www.acer.com/ua-uk/support">--}}
{{--                                            Рішення--}}
{{--                                        </a>--}}
{{--                                        <ul class="menu__submenu is-drilldown__submenu">--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/education">--}}
{{--                                                    Acer for Education--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://emea-acerforbusiness.acer.com/" rel="nofollow">--}}
{{--                                                    Acer for Business--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/travelmate-solutions">--}}
{{--                                                    TravelMate--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/antimicrobial">--}}
{{--                                                    Антимікробні рішення--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/military-standards">--}}
{{--                                                    Армійські стандарти--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/windows-11">--}}
{{--                                                    Windows 11--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu__item--mobile">--}}
{{--                                                <a class="menu__link--mobile"--}}
{{--                                                   href="https://www.acer.com/ua-uk/chromeos-enterprise">--}}
{{--                                                    ChromeOS Enterprise--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}
{{--                                    </li>--}}
{{--                                    <li class="menu__item--mobile"--}}
{{--                                    >--}}
{{--                                        <a class="menu__link--mobile" id="products"--}}
{{--                                           href="https://www.acer.com/ua-uk/support" data-toggle="sub-menu">--}}
{{--                                            Підтримка--}}

{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li class="menu__item--mobile"--}}
{{--                                    >--}}
{{--                                        <a class="menu__link--mobile" id="products"--}}
{{--                                           href="https://www.acer.com/ua-uk/events" data-toggle="sub-menu">--}}
{{--                                            Події--}}

{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="nav-mobile__location">--}}
{{--                            <ul class="menu menu--vertical">--}}
{{--                                <li class="menu__item">--}}
{{--                                    <a class="menu__link"--}}
{{--                                       href="https://www.acer.com/ua-uk/languages">--}}
{{--                                        <span class="material-icons">language</span>--}}
{{--                                        Мова--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                        <div class="nav-mobile__brands">--}}
{{--                            <p style="padding: 1rem; margin: 0;font-size: 12px;line-height: 17px;"--}}
{{--                               class="nav-mobile__text">Дивитися наші бренди</p>--}}
{{--                            <ul class="menu menu--vertical">--}}
{{--                                <li class="menu__item"><a class="menu__item"--}}
{{--                                                          href="https://www.acer.com/ua-uk" target="_self"--}}
{{--                                                          property="linkText" rel="nofollow">Acer</a></li>--}}
{{--                                <li class="menu__item"><a class="menu__item"--}}
{{--                                                          href="https://www.acer.com/ua-uk/predator" target="_blank"--}}
{{--                                                          property="linkText" rel="nofollow">Predator</a></li>--}}
{{--                                <li class="menu__item"><a class="menu__item"--}}
{{--                                                          href="https://www.acer.com/ua-uk/conceptd" target="_blank"--}}
{{--                                                          property="linkText" rel="nofollow">ConceptD</a></li>--}}
{{--                                <li class="menu__item"><a class="menu__item"--}}
{{--                                                          href="https://www.planet9.gg/" target="_blank"--}}
{{--                                                          property="linkText" rel="nofollow">Planet9</a></li>--}}
{{--                                <li class="menu__item"><a class="menu__item"--}}
{{--                                                          href="https://www.acer.com/ua-uk/SpatialLabs" target="_blank"--}}
{{--                                                          property="linkText" rel="nofollow">SpatialLabs</a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </nav>--}}
{{--        </div>--}}
{{--        <div class="slideout--background"></div>--}}
{{--    </div>--}}
</header>

<div class="pt pt--left-rail pt--search">
    <div class="region-hero">

        {{--
        <div class="breadcrumb-wrapper">
        </div>
        --}}

        <section class="section section--secondary">
            <div class="section__body--search section__body--flush">
                <section class="hero hero--search hero--center">
                    <div class="hero__caption hero__caption--100 hero__caption--center">
                        <h1 class="hero__title h2">Підтримка</h1>
                        <p class="hero__text">Введіть свій серійний номер, щоб переглянути інформацію про гарантію для вашого продукту Acer.</p>
                        <div class="search-box">
                            <form class="search-box__form" action="" method="GET">
                                <input type="text" class="search-box__input ui-autocomplete-input"
                                       placeholder="Що допомогти вам знайти" name="search"  value="{{old("search")}}"
                                       maxlength="50" autocomplete="off">
                                <p class="support-dm__label" id="error_msgs"
                                   data-msgs="[&quot;Номер частини не знайдено.&quot;,&quot;Серійний номер не знайдено.&quot;,&quot;SNID не знайдено.&quot;]"></p>
                            </form>
                        </div>
                    </div>
                    <picture class="hero__image hero__image--100" style="object-position:50% 50%">
                        <source media="(max-width: 375px)"
                                srcset="{{asset('acer/support-search-banner-1_Search-Hero-Component-S.webp')}}">
                        <source media="(min-width: 376px) and (max-width: 640px)"
                                srcset="{{asset('acer/support-search-banner-1_Search-Hero-Component-M2.webp')}}">
                        <source media="(min-width: 641px) and (max-width: 899px)"
                                srcset="{{asset('acer/support-search-banner-1_Search-Hero-Component-M1.webp')}}">
                        <source media="(min-width: 900px) and (max-width: 1366px)"
                                srcset="{{asset('acer/support-search-banner-1_Search-Hero-Component-L.webp')}}">
                        <source media="(min-width: 1367px)"
                                srcset="{{asset('acer/support-search-banner-1_Search-Hero-Component-XL.webp')}}">
                        <img class=""
                             src="{{asset('acer/support-search-banner-1_Search-Hero-Component-S.webp')}}"
                             alt="support-search-banner" title="" style="object-position:50% 50%"></picture>
                </section>
            </div>
        </section>
        @yield('content')


    </div>
</div>


<footer class="footer">
    <section class="footer-bottom">
        <nav class="nav-legal" aria-label="Footer Legal Navigation Menu">
            <ul class="menu menu--legal">
                <li class=""><a
                        target="" href="https://www.acer.com/ua-uk/privacy">Конфіденційність</a></li>
                <li class=""><a
                        target="" href="https://www.acer.com/ua-uk/privacy/cookie-policy">Політика використання файлів
                        cookie</a></li>
                <li class=""><a
                        target="_blank" href="https://www.acer.com/legal" rel="nofollow">Правові положення</a></li>
                <li class=""><a
                        target="" href="https://www.acer.com/ua-uk/privacy/additional-legal-information">Додаткова
                        юридична інформація</a></li>
                <li>
                    <a class="ot-sdk-show-settings" title="Cookie Settings"
                       style="cursor: pointer;">Налаштування cookie</a>
                </li>
            </ul>
        </nav>

        <div class="social">
            <ul class="social__list">
                <li class="social__item">
                    <a class="social-list__link"
                       target="_blank" href="https://www.facebook.com/AcerUkraine/" title="Facebook" rel="nofollow">
                        <svg class="icon icon-white">
                            <use xlink:href="#facebook"></use>
                        </svg>
                    </a>
                </li>
                <li class="social__item">
                    <a class="social-list__link"
                       target="_blank" href="https://www.instagram.com/acerukraine/" title="Instagram" rel="nofollow">
                        <svg class="icon icon-white">
                            <use xlink:href="#instagram"></use>
                        </svg>
                    </a>
                </li>
                <li class="social__item">
                    <a class="social-list__link"
                       target="_blank" href="https://twitter.com/acer_ukraine" title="Twitter" rel="nofollow">
                        <svg class="icon icon-white">
                            <use xlink:href="#twitter"></use>
                        </svg>
                    </a>
                </li>
                <li class="social__item">
                    <a class="social-list__link"
                       target="_blank" href="https://www.youtube.com/channel/UC1GiOhiRcevyZ9CRvF9IbEw" title="YouTube"
                       rel="nofollow">
                        <svg class="icon icon-white">
                            <use xlink:href="#youtube"></use>
                        </svg>
                    </a>
                </li>
            </ul>
        </div>
        <div class="copyright">
            <p class="copyright__text">© 2023 Acer Inc.</p>
        </div>
        <div class="locale">


            <a href="https://www.acer.com/ua-uk/languages" target=""
               class="locale__text">
        <span class="material-icons">
            <img src="{{asset('acer/language_white.svg')}}" alt="language'">
        </span>Україна
            </a>
        </div>

    </section>


    <button id="btt" class="btn btn--btt">
        <svg class="material-icons" xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px"
             fill="#80c343">
            <path d="M0 0h24v24H0V0z" fill="none"></path>
            <path d="M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6-6 6 1.41 1.41z"></path>
        </svg>
    </button>


</footer>


<script src="{{asset('acer/main.bundle.js')}}"></script>
<script src="{{asset('acer/utility.js')}}"></script>

<div class="loader-wrapper" style="display: none;">
    <div class="spinner">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
</body>
</html>
