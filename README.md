***Developed by Herman Kramar***

<h1>Deploy guide:</h1>

<ul>
    <li>
        Clone this repo
    </li>
    <li>
        APP_URL -> <b><i>SHOULD BE CORRECT URL OF APPLICATION FOR CORRECT SERVING IMAGES</i></b>
        <br/>
        <code>
            cp .env.example .env
        </code>
    </li>
    <li>
        Change .env variable according to your needs
    </li>
    <li>
        # run <br/>
        <code>docker-compose build</code>
    </li>
    <li>
        # run <br/>
        <code>docker-compose up -d</code>
    </li>
    <li>
        # run <br/>
        <code>docker-compose exec app composer install</code>
    </li>
    <li>
        # run <br/>
        <code>docker-compose exec app php artisan key:generate</code>
    </li>
    <li>
        # run for initial database migration<br/>
        <code>docker-compose exec app php artisan migrate</code>
    </li>
    <li>
        # run for linking private storage for images to public access<br/>
        <code>docker-compose exec app php artisan storage:link</code>
    </li>
    <li>
        # run <br/>
        <code>docker-compose exec app php artisan make:filament-user</code>
        <br/>
        <b>by this command you create user for access to admin panel</b>
    </li>
</ul>

<h2>That's all, enjoy :)</h2>
