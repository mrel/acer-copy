(function () {
var cdn = 'https://cdn.admixer.net/analytics/'; //'https://inv-nets-stage.admixer.net/analytics/';//
var sModules = '';
var runtimeSrc = cdn + 'tag-manager.runtime.js';
var tagmanSrc = cdn + 'tag-manager.js?m=' + sModules;
function appendScript(src) {
var script = document.createElement('script');
script.src = src;
script.async = true;
document.head.appendChild(script);
}
function appendRuntime() {
var scripts = document.getElementsByTagName('script');
for (var i = 0, ln = scripts.length; i < ln; i++) {
if (scripts[i].src === runtimeSrc) {
return;
}
}
appendScript(runtimeSrc);
}
appendRuntime();
appendScript(tagmanSrc);
(window.admixTMLoad = window.admixTMLoad || []).push({
setConfig: {
dataStorage: {
keys: [

]
},
dataFromUrl: { enabled: true },
urlChange: {
emitOn: ['path','search']
},

},
setPixel: [
'https://inv-nets-eu.admixer.net/dmpapxl.aspx?cntoid=cc10e779-6847-4aa5-b7ae-97007a2407a3&referrer=%%referrer%%&page=%%pageUrl%%&fp_am_uid=%%fpamuid%%&bot=%%bot%%',


],
setData: {"containerOId":"cc10e779-6847-4aa5-b7ae-97007a2407a3"}
,
setDataHandler: 'https://inv-nets-eu.admixer.net/cntdata.aspx?query=',
});
}) ();